create table if not exists oauth_access_token (
 token_id VARCHAR(255),
 token bytea,
 authentication_id VARCHAR(255) PRIMARY KEY,
 user_name VARCHAR(255),
 client_id VARCHAR(255),
 authentication bytea,
 refresh_token VARCHAR(255)
);

create table if not exists oauth_refresh_token (
 token_id VARCHAR(255),
 token bytea,
 authentication bytea
);

--
-- Structure de la table tbl_inventory_products
--

drop table if exists tbl_inventory_products;
create table if not exists tbl_inventory_products (
  inventory_id BIGINT NOT NULL,
  quantity BIGINT ,
  product_id BIGINT NOT NULL
);

--
-- Structure de la table tbl_product
--

drop table if exists tbl_product;
create table if not exists tbl_product (
  id BIGINT NOT NULL,
  current_quantity BIGINT,
  description VARCHAR(255),
  enabled BOOLEAN,
  name VARCHAR(255),
  warehouse_id BIGINT 
);

--
-- Structure de la table tbl_inventory
--

drop table if exists tbl_inventory;
create table if not exists tbl_inventory (
  id BIGINT NOT NULL,
  closed BOOLEAN,
  date TIMESTAMP,
  description VARCHAR(255),
  name VARCHAR(255),
  author_id BIGINT,
  warehouse_id BIGINT 
);

--
-- Structure de la table tbl_warehouse_permissions
--

drop table if exists tbl_warehouse_permissions;
create table if not exists tbl_warehouse_permissions (
  warehouse_id BIGINT NOT NULL,
  permission VARCHAR(255),
  user_id BIGINT NOT NULL
);

--
-- Structure de la table tbl_warehouse
--

drop table if exists tbl_warehouse;
create table if not exists tbl_warehouse (
  id BIGINT NOT NULL,
  name VARCHAR(255),
  creator_id BIGINT 
);

--
-- Structure de la table tbl_user
--

drop table if exists tbl_user;
create table if not exists tbl_user (
  id BIGINT NOT NULL,
  email VARCHAR(255),
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  password VARCHAR(255) 
);

--
-- Sequences
--

drop sequence if exists user_seq;
create sequence if not exists user_seq start 5;

drop sequence if exists product_seq;
create sequence if not exists product_seq start 13;

drop sequence if exists inventory_seq;
create sequence if not exists inventory_seq start 12;

drop sequence if exists warehouse_seq;
create sequence if not exists warehouse_seq start 4;