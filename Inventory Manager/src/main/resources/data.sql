--
-- Contenu de la table tbl_user
--

INSERT INTO tbl_user (id, email, first_name, last_name, password) VALUES
(1, 'jean.dupont@gmail.com', 'Jean', 'Dupont', '{bcrypt}$2a$10$l4M92QNLjJG2SUDKcRyfrOpetqXrLNBto2FJD344BbXacVe/qJL5a'),
(2, 'sophie.dupont@gmail.com', 'Sophie', 'Dupont', '{bcrypt}$2a$10$p8LJzfzT6wt.jAP4tL79f.h37Nqak8GPQ6qvm2SMEPiu2tolXzIEO'),
(3, 'charles.dupont@gmail.com', 'Charles', 'Dupont', '{bcrypt}$2a$10$BDnA6F2xJ1gF6L7z2xjLpOIyB0QWI5ObLeLxpze1J0WDiTwO97KmC'),
(4, 'david.dupont@gmail.com', 'David', 'Dupont', '{bcrypt}$2a$10$vj6SN1u9zw9PeSliA5ljA.xv.GJVCP2TIGIlk1pmXTy/TumYlJKz2');

--
-- Contenu de la table tbl_warehouse
--

INSERT INTO tbl_warehouse (id, name, creator_id) VALUES
(1, 'Leclerc', 1),
(2, 'Carrefour', 2),
(3, 'Auchan', 2);

--
-- Contenu de la table tbl_inventory
--

INSERT INTO tbl_inventory (id, closed, date, description, name, author_id, warehouse_id) VALUES
(1, true, '2019-01-28 00:35:01', 'Num. 12635', 'Inventaire #1', 1, 1),
(2, true, '2019-01-28 00:35:34', 'Num. 12004', 'Inventaire #2', 1, 1),
(3, true, '2019-01-28 00:36:09', 'Num. 96853', 'Inventaire #3', 1, 1),
(4, false, '2019-01-28 00:36:43', 'Num. 00236', 'Inventaire #4', 1, 1),
(5, true, '2019-01-28 00:37:35', 'Num. 11245', 'Inventaire #5', 1, 1),
(6, true, '2019-01-28 00:38:44', 'Num. 77856', 'Inventaire #1', 1, 2),
(7, true, '2019-01-28 00:39:09', 'Num. 75512', 'Inventaire #2', 1, 2),
(8, false, '2019-01-28 00:39:27', 'Num. 52326', 'Inventaire #3', 1, 2),
(9, true, '2019-01-28 00:40:50', 'Num. 52525', 'Inventaire #6', 2, 1),
(10, true, '2019-01-28 00:41:21', 'Num. 85960', 'Inventaire #7', 2, 1),
(11, false, '2019-01-28 00:41:55', 'Num. 00230', 'Inventaire #8', 2, 1);

--
-- Contenu de la table tbl_product
--

INSERT INTO tbl_product (id, current_quantity, description, enabled, name, warehouse_id) VALUES
(1, 0, 'Code 4526330', true, 'Produit #1', 1),
(2, NULL, 'Code 1200523', true, 'Produit #2', 1),
(3, 19, 'Code 9886532', true, 'Produit #3', 1),
(4, 10, 'Code 8854523', true, 'Produit #4', 1),
(5, 18, 'Code 7700256', true, 'Produit #5', 1),
(6, 13, 'Code 9958020', true, 'Produit #6', 1),
(7, 12, 'Code 2001256', true, 'Produit #7', 1),
(8, 18, 'Code 0021235', true, 'Produit #8', 1),
(9, 20, 'Code 5552036', true, 'Produit #1', 2),
(10, 50, 'Code 9963259', true, 'Produit #2', 2),
(11, 9, 'Code 1474120', true, 'Produit #3', 2),
(12, 0, 'Code 1236325', true, 'Produit #4', 2);

--
-- Contenu de la table tbl_inventory_products
--

INSERT INTO tbl_inventory_products (inventory_id, quantity, product_id) VALUES
(1, 17, 1),
(1, 29, 3),
(1, 1, 4),
(2, 13, 1),
(2, 16, 6),
(2, 20, 7),
(3, 8, 7),
(3, 50, 8),
(4, 13, 1),
(4, 35, 3),
(4, 2, 4),
(5, 0, 4),
(5, 9, 6),
(5, 32, 7),
(5, 0, 8),
(6, 14, 9),
(6, 1, 10),
(6, 9, 11),
(6, 0, 12),
(7, 20, 9),
(7, 50, 10),
(9, 19, 3),
(9, 6, 4),
(9, 18, 5),
(10, 0, 1),
(10, 10, 4),
(10, 13, 6),
(10, 12, 7),
(11, 18, 8);

--
-- Contenu de la table tbl_warehouse_permissions
--

INSERT INTO tbl_warehouse_permissions (warehouse_id, permission, user_id) VALUES
(1, 'ADMIN', 2),
(1, 'USER', 3),
(1, 'USER', 4),
(2, 'USER', 1),
(3, 'ADMIN', 1),
(3, 'ADMIN', 4);