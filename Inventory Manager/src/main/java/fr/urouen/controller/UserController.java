package fr.urouen.controller;

import fr.urouen.dto.form.EditPasswordFormDto;
import fr.urouen.dto.form.EditProfileFormDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/users")
@Api(value = "User Controller", description = "Operations about users")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "Get the user information of the authenticated user", response = UserDto.class)
    @GetMapping("profile")
    public ResponseEntity<UserDto> getMyProfile() {
        return ResponseEntity.ok(userService.findMyself());
    }

    @ApiOperation(value = "Get the user information from a specific email", response = UserDto.class)
    @GetMapping
    public ResponseEntity<?> getUserWithEmail(@RequestParam String email) {
        return ResponseEntity.ok(userService.findByEmail(email));
    }

    @ApiOperation(value = "Update the profile information of the authenticated user", response = UserDto.class)
    @PutMapping("profile/information")
    public ResponseEntity<?> updateProfile(@ApiParam(value = "Profile information", required = true) @Valid @RequestBody EditProfileFormDto dto) {
        return ResponseEntity.ok(userService.updateInformation(dto));
    }

    @ApiOperation(value = "Update the password of the authenticated user", response = UserDto.class)
    @PutMapping("profile/password")
    public ResponseEntity<?> updatePassword(@ApiParam(value = "Password information", required = true) @Valid @RequestBody EditPasswordFormDto dto) {
        return ResponseEntity.ok(userService.updatePassword(dto));
    }
}