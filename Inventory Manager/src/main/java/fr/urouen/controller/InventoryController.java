package fr.urouen.controller;

import fr.urouen.dto.ResponseDto;
import fr.urouen.dto.form.InventoryFormDto;
import fr.urouen.dto.inventory.InventoryDto;
import fr.urouen.service.InventoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/inventories")
@Api(value = "Inventory Controller", description = "Operations about inventories")
public class InventoryController {
    @Autowired
    private InventoryService inventoryService;

    @ApiOperation(value = "Get an inventory", response = InventoryDto.class)
    @GetMapping("{id}")
    public ResponseEntity<?> getInventoryWithId(@PathVariable Long id) {
        return ResponseEntity.ok(inventoryService.findById(id));
    }

    @ApiOperation(value = "List all inventories created by the authenticated user inside a warehouse", response = InventoryDto[].class)
    @GetMapping(value = "mine", params = "fromWarehouse")
    public ResponseEntity<?> getMyInventoriesFromWarehouse(@RequestParam(value = "fromWarehouse") Long warehouseId) {
        return ResponseEntity.ok(inventoryService.findMyInventoriesByWarehouseId(warehouseId));
    }

    @ApiOperation(value = "List full information about an inventory", response = InventoryDto.class)
    @GetMapping("{id}/products")
    public ResponseEntity<?> getInventoryProductsWithId(@PathVariable Long id) {
        return ResponseEntity.ok(inventoryService.findInventoryProductsById(id));
    }

    @ApiOperation(value = "List the five last closed inventories of a warehouse", response = InventoryDto[].class)
    @GetMapping("latest")
    public ResponseEntity<?> getLatestInventoriesFromWarehouse(@RequestParam(value = "fromWarehouse") Long warehouseId) {
        return ResponseEntity.ok(inventoryService.findLatestByWarehouseId(warehouseId));
    }

    @ApiOperation(value = "List all inventories inside a warehouse", response = InventoryDto[].class)
    @GetMapping(params = "fromWarehouse")
    public ResponseEntity<?> getInventoriesFromWarehouse(@RequestParam(value = "fromWarehouse") Long warehouseId) {
        return ResponseEntity.ok(inventoryService.findByWarehouseId(warehouseId));
    }

    @ApiOperation(value = "Create a new inventory inside a warehouse", response = InventoryDto.class)
    @PostMapping(params = "inWarehouse")
    public ResponseEntity<?> create(@RequestParam(value = "inWarehouse") Long warehouseId, @ApiParam(value = "Inventory which will be created", required = true) @RequestBody InventoryFormDto inventory) {
        return ResponseEntity.ok(inventoryService.create(inventory, warehouseId));
    }

    @ApiOperation(value = "Update an inventory", response = InventoryDto.class)
    @PutMapping("{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @ApiParam(value = "Inventory which will be updated", required = true) @RequestBody InventoryFormDto inventory) {
        return ResponseEntity.ok(inventoryService.update(id, inventory));
    }

    @ApiOperation(value = "Delete an inventory", response = ResponseDto.class)
    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        inventoryService.delete(id);

        return ResponseEntity.ok(ResponseDto.create("L'inventaire a été supprimé"));
    }
}