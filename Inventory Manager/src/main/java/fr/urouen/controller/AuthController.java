package fr.urouen.controller;

import fr.urouen.dto.form.SignUpFormDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "auth")
@Api(value = "Auth Controller", description = "Operations about sign up")
public class AuthController {
    @Autowired
    private UserService userService;

    @PostMapping("signup")
    @ApiOperation(value = "Sign up a new user", response = UserDto.class)
    public ResponseEntity<?> signUp(@ApiParam(value = "User account informations", required = true) @RequestBody SignUpFormDto user) {
        return ResponseEntity.ok(userService.create(user));
    }
}