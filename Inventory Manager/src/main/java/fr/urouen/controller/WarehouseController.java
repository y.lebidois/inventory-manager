package fr.urouen.controller;

import fr.urouen.dto.ResponseDto;
import fr.urouen.dto.form.WarehouseFormDto;
import fr.urouen.dto.warehouse.WarehouseDto;
import fr.urouen.dto.warehouse.WarehousePermissionDto;
import fr.urouen.dto.warehouse.WarehouseStatisticsDto;
import fr.urouen.service.WarehouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/warehouses")
@Api(value = "Warehouse Controller", description = "Operations about warehouses")
public class WarehouseController {
    @Autowired
    private WarehouseService warehouseService;

    @ApiOperation(value = "List all the warehouses were the authenticated user have an access", response = WarehouseDto[].class)
    @GetMapping
    public ResponseEntity<?> getWarehouses() {
        return ResponseEntity.ok(warehouseService.getMyWarehouses());
    }

    @ApiOperation(value = "Get the statistics of a warehouse", response = WarehouseStatisticsDto.class)
    @GetMapping("{id}/statistics")
    public ResponseEntity<?> getStatistics(@PathVariable Long id) {
        return ResponseEntity.ok(warehouseService.getStatistics(id));
    }

    @ApiOperation(value = "Create a warehouse", response = WarehouseDto.class)
    @PostMapping
    public ResponseEntity<?> create(@ApiParam(value = "Warehouse which will be created", required = true) @Valid @RequestBody WarehouseFormDto warehouse) {
        return ResponseEntity.ok(warehouseService.create(warehouse));
    }

    @ApiOperation(value = "List all the permissions of a warehouse", response = WarehousePermissionDto[].class)
    @GetMapping("{id}/permissions")
    public ResponseEntity<?> getPermissions(@PathVariable Long id) {
        return ResponseEntity.ok(warehouseService.getPermissions(id));
    }

    @ApiOperation(value = "Create a permission inside a warehouse", response = WarehousePermissionDto.class)
    @PostMapping("{id}/permissions")
    public ResponseEntity<?> createPermission(@PathVariable Long id, @ApiParam(value = "Permission which will be added to the warehouse", required = true) @Valid @RequestBody WarehousePermissionDto permission) {
        return ResponseEntity.ok(warehouseService.createPermission(id, permission));
    }

    @ApiOperation(value = "Update a permission from a warehouse", response = WarehousePermissionDto.class)
    @PutMapping("{id}/permissions")
    public ResponseEntity<?> updatePermission(@PathVariable Long id, @ApiParam(value = "Permission which will be updated from the warehouse", required = true) @Valid @RequestBody WarehousePermissionDto permission) {
        return ResponseEntity.ok(warehouseService.updatePermission(id, permission));
    }

    @ApiOperation(value = "Delete a permission from a warehouse", response = ResponseDto.class)
    @DeleteMapping("{id}/permissions")
    public ResponseEntity<?> deletePermission(@PathVariable Long id, @ApiParam(value = "Permission which will be removed from the warehouse", required = true) @Valid @RequestBody WarehousePermissionDto permission) {
        warehouseService.deletePermission(id, permission);
        return ResponseEntity.ok(ResponseDto.create("La permission a été supprimé"));
    }
}