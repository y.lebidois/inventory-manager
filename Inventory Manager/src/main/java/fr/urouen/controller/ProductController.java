package fr.urouen.controller;

import fr.urouen.dto.ResponseDto;
import fr.urouen.dto.form.ProductFormDto;
import fr.urouen.dto.product.ProductDto;
import fr.urouen.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/products")
@Api(value = "Product Controller", description = "Operations about products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @ApiOperation(value = "Create a product inside a warehouse", response = ProductDto.class)
    @PostMapping(params = "inWarehouse")
    public ResponseEntity<?> createProductInWarehouse(@RequestParam(value = "inWarehouse") Long warehouseId, @ApiParam(value = "Product which will be created", required = true) @Valid @RequestBody ProductFormDto product) {
        return ResponseEntity.ok(productService.create(product, warehouseId));
    }

    @ApiOperation(value = "List all products from a warehouse", response = ProductDto[].class)
    @GetMapping(params = "fromWarehouse")
    public ResponseEntity<?> getProductsFromWarehouse(@RequestParam(value = "fromWarehouse") Long warehouseId) {
        return ResponseEntity.ok(productService.findByWarehouseId(warehouseId));
    }

    @ApiOperation(value = "Update a product", response = ProductDto.class)
    @PutMapping("{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @ApiParam(value = "Product which will be updated", required = true) @Valid @RequestBody ProductFormDto product) {
        return ResponseEntity.ok(productService.update(id, product));
    }

    @ApiOperation(value = "Delete a product")
    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        productService.delete(id);

        return ResponseEntity.ok(ResponseDto.create("Le produit a été supprimé"));
    }
}