package fr.urouen.config;

import fr.urouen.entity.InventoryEntity;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.InventoryRepository;
import fr.urouen.repository.WarehouseRepository;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Optional;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        final CustomMethodSecurityExpressionHandler expressionHandler = new CustomMethodSecurityExpressionHandler();
        return expressionHandler;
    }

    class CustomMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {
        private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

        @Override
        protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, MethodInvocation invocation) {
            CustomMethodSecurityExpressionRoot root = new CustomMethodSecurityExpressionRoot(authentication);
            root.setPermissionEvaluator(getPermissionEvaluator());
            root.setTrustResolver(this.trustResolver);
            root.setRoleHierarchy(getRoleHierarchy());
            return root;
        }
    }

    class CustomMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
        private Object filterObject;
        private Object returnObject;

        public CustomMethodSecurityExpressionRoot(Authentication authentication) {
            super(authentication);
        }

        public boolean hasWarehouseUserPermission(Long warehouseId) {
            UserEntity user = ((UserEntity) this.getPrincipal());

            Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

            // If the warehouse does not exist, no access
            if (!warehouse.isPresent()) {
                return false;
            }

            // If the user is the creator of the warehouse or has user permission
            return warehouse.get().getCreator().equals(user) ||
                    warehouse.get().getUserPermissions().containsKey(user);
        }

        public boolean hasWarehouseAdminPermission(Long warehouseId) {
            UserEntity user = ((UserEntity) this.getPrincipal());

            Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

            // If the warehouse does not exist, no access
            if (!warehouse.isPresent()) {
                return false;
            }

            WarehouseEntity.UserPermission permission = warehouse.get().getUserPermissions().get(user);

            // If the user is the creator of the warehouse or has admin permission
            return warehouse.get().getCreator().equals(user) ||
                    (permission != null && permission == WarehouseEntity.UserPermission.ADMIN);
        }

        public boolean hasInventoryReadPermission(Long inventoryId) {
            UserEntity user = ((UserEntity) this.getPrincipal());

            Optional<InventoryEntity> optional = inventoryRepository.findById(inventoryId);

            // If the inventory does not exist, no access
            if (!optional.isPresent()) {
                return false;
            }

            InventoryEntity entity = optional.get();

            // If the user is the author of this inventory, access!
            if (entity.getAuthor().equals(user)) {
                return true;
            }

            if (entity.getWarehouse().getCreator().equals(user)) {
                return true;
            }

            WarehouseEntity.UserPermission permission = entity.getWarehouse().getUserPermissions().get(user);

            // If the user has no permission on the warehouses, no access
            if (permission == null) {
                return false;
            }

            // If the user is admin on the warehouse and the inventory is closed, access!
            if (permission == WarehouseEntity.UserPermission.ADMIN && entity.isClosed()) {
                return true;
            }

            // If the inventory is showed on home page, access!
            List<InventoryEntity> lastInventories = inventoryRepository.findTop5ByWarehouseIdAndClosedIsTrueOrderByDateDesc(entity.getWarehouse().getId());
            return lastInventories.contains(entity);
        }

        public boolean hasInventoryWritePermission(Long inventoryId) {
            UserEntity user = ((UserEntity) this.getPrincipal());

            Optional<InventoryEntity> inventory = inventoryRepository.findById(inventoryId);

            // If the inventory does not exist, no access
            if (!inventory.isPresent()) {
                return false;
            }

            // If the user is the author of this inventory, access!
            return inventory.get().getAuthor().equals(user);
        }

        @Override
        public void setFilterObject(Object filterObject) {
            this.filterObject = filterObject;
        }

        @Override
        public Object getFilterObject() {
            return filterObject;
        }

        @Override
        public void setReturnObject(Object returnObject) {
            this.returnObject = returnObject;
        }

        @Override
        public Object getReturnObject() {
            return returnObject;
        }

        @Override
        public Object getThis() {
            return this;
        }
    }
}