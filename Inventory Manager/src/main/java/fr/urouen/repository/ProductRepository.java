package fr.urouen.repository;

import fr.urouen.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    List<ProductEntity> findByWarehouseIdAndEnabledIsTrueOrderByName(Long id);
}