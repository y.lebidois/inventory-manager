package fr.urouen.repository;

import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface WarehouseRepository extends JpaRepository<WarehouseEntity, Long> {
    @Query(value = "SELECT w.*\n" +
            "FROM tbl_warehouse w\n" +
            "INNER JOIN tbl_user u ON w.creator_id = u.id\n" +
            "WHERE u.id = ?1\n" +
            "UNION\n" +
            "SELECT w.*\n" +
            "FROM tbl_warehouse w\n" +
            "INNER JOIN tbl_warehouse_permissions p ON w.id = p.warehouse_id\n" +
            "WHERE p.user_id = ?1", nativeQuery = true)
    List<WarehouseEntity> findAccessiblesWarehouses(Long userId);
}