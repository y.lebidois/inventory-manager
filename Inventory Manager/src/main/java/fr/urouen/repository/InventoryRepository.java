package fr.urouen.repository;

import fr.urouen.entity.InventoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventoryRepository extends JpaRepository<InventoryEntity, Long> {
    List<InventoryEntity> findByAuthorIdOrderByDateDesc(Long id);
    List<InventoryEntity> findByWarehouseIdAndClosedIsTrueOrderByDateDesc(Long id);
    List<InventoryEntity> findTop5ByWarehouseIdAndClosedIsTrueOrderByDateDesc(Long id);
}