package fr.urouen.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "tbl_warehouse")
public class WarehouseEntity {
    @Id
    @SequenceGenerator(name = "warehouse_generator", sequenceName = "warehouse_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="warehouse_generator")
    private Long id;

    private String name;

    @OneToOne
    private UserEntity creator;

    @OneToMany(mappedBy = "warehouse")
    private Collection<InventoryEntity> inventories;

    @OneToMany(mappedBy = "warehouse")
    private Collection<ProductEntity> products;

    @ElementCollection
    @CollectionTable(name = "tbl_warehouse_permissions", joinColumns = @JoinColumn(name = "warehouse_id"))
    @MapKeyJoinColumn(name = "user_id")
    @Column(name = "permission")
    @Enumerated(EnumType.STRING)
    private Map<UserEntity, UserPermission> userPermissions;

    public WarehouseEntity() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UserEntity getCreator() {
        return creator;
    }

    public Map<UserEntity, UserPermission> getUserPermissions() {
        return userPermissions;
    }

    public Collection<InventoryEntity> getInventories() {
        return inventories;
    }

    public Collection<ProductEntity> getProducts() {
        return products;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreator(UserEntity creator) {
        this.creator = creator;
    }

    public void setUserPermissions(Map<UserEntity, UserPermission> userPermissions) {
        this.userPermissions = userPermissions;
    }

    public void setInventories(Collection<InventoryEntity> inventories) {
        this.inventories = inventories;
    }

    public void setProducts(Collection<ProductEntity> products) {
        this.products = products;
    }

    public enum UserPermission {
        USER, ADMIN;

        public static UserPermission getUserPermission(String permission) {
            return permission.equals("ADMIN") ? ADMIN : USER;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WarehouseEntity that = (WarehouseEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}