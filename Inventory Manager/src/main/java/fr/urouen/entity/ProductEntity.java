package fr.urouen.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tbl_product")
public class ProductEntity {
    @Id
    @SequenceGenerator(name = "product_generator", sequenceName = "product_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="product_generator")
    private Long id;

    private String name;
    private String description;
    private Long currentQuantity;
    private Boolean enabled;

    @ManyToOne
    private WarehouseEntity warehouse;

    public ProductEntity() {
    }

    public ProductEntity(Long id, String name, String description, Long currentQuantity, boolean enabled, WarehouseEntity warehouse) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.currentQuantity = currentQuantity;
        this.enabled = enabled;
        this.warehouse = warehouse;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Long getCurrentQuantity() {
        return currentQuantity;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public WarehouseEntity getWarehouse() {
        return warehouse;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCurrentQuantity(Long currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setWarehouse(WarehouseEntity warehouse) {
        this.warehouse = warehouse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}