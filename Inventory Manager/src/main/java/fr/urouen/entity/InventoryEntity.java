package fr.urouen.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "tbl_inventory")
public class InventoryEntity {
    @Id
    @SequenceGenerator(name = "inventory_generator", sequenceName = "inventory_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="inventory_generator")
    private Long id;

    private String name;

    private String description;

    @ManyToOne
    private UserEntity author;

    private LocalDateTime date;

    @ElementCollection
    @CollectionTable(name = "tbl_inventory_products", joinColumns = @JoinColumn(name = "inventory_id"))
    @MapKeyJoinColumn(name = "product_id")
    @Column(name = "quantity")
    private Map<ProductEntity, Long> products;

    @ManyToOne
    private WarehouseEntity warehouse;

    private Boolean closed;

    public InventoryEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Map<ProductEntity, Long> getProducts() {
        return products;
    }

    public void setProducts(Map<ProductEntity, Long> products) {
        this.products = products;
    }

    public WarehouseEntity getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WarehouseEntity warehouse) {
        this.warehouse = warehouse;
    }

    public Boolean isClosed() {
        return closed;
    }

    @Transient
    public Boolean isOutOfStock() {
        if (!closed) {
            return false;
        }

        for (Long quantity : products.values()) {
            if (quantity == 0) {
                return true;
            }
        }

        return false;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryEntity entity = (InventoryEntity) o;
        return Objects.equals(id, entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}