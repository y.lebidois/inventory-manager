package fr.urouen.dto.warehouse;

public class WarehouseStatisticsAdminDto extends WarehouseStatisticsDto {
    private String status;
    private int usersCount;
    private int inventoriesClosedCount;
    private int outOfStockCount;

    public WarehouseStatisticsAdminDto() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(int usersCount) {
        this.usersCount = usersCount;
    }

    public int getInventoriesClosedCount() {
        return inventoriesClosedCount;
    }

    public void setInventoriesClosedCount(int inventoriesClosedCount) {
        this.inventoriesClosedCount = inventoriesClosedCount;
    }

    public int getOutOfStockCount() {
        return outOfStockCount;
    }

    public void setOutOfStockCount(int outOfStockCount) {
        this.outOfStockCount = outOfStockCount;
    }
}
