package fr.urouen.dto.warehouse;

import fr.urouen.dto.user.UserDto;

public class WarehousePermissionDto {
    private UserDto user;
    private boolean admin;

    public WarehousePermissionDto() {
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}