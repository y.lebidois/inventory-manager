package fr.urouen.dto.warehouse;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class WarehouseStatisticsDto {
    private LocalDateTime lastClosedDate;
    private int pendingInventoriesCount;
    private int productsCount;
    private int myInventoriesCount;

    public WarehouseStatisticsDto() {
    }

    public LocalDateTime getLastClosedDate() {
        return lastClosedDate;
    }

    public void setLastClosedDate(LocalDateTime lastClosedDate) {
        this.lastClosedDate = lastClosedDate;
    }

    public int getPendingInventoriesCount() {
        return pendingInventoriesCount;
    }

    public void setPendingInventoriesCount(int pendingInventoriesCount) {
        this.pendingInventoriesCount = pendingInventoriesCount;
    }

    public int getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(int productsCount) {
        this.productsCount = productsCount;
    }

    public int getMyInventoriesCount() {
        return myInventoriesCount;
    }

    public void setMyInventoriesCount(int myInventoriesCount) {
        this.myInventoriesCount = myInventoriesCount;
    }
}
