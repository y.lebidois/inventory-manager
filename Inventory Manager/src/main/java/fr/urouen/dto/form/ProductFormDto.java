package fr.urouen.dto.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ProductFormDto {
    @NotBlank
    @Size(min = 1, max = 30, message = "Le nom doit contenir entre 1 et 30 caractères")
    private String name;

    @Size(min = 0, max = 100, message = "La description doit contenir au maximum 100 caractères")
    private String description;

    public ProductFormDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
