package fr.urouen.dto.form;

import javax.validation.constraints.NotBlank;

public class LoginFormDto {
    @NotBlank(message = "L'adresse email est un champ obligatoire")
    private String mail;

    @NotBlank(message = "Le mot de passe est un champ obligatoire")
    private String password;

    public String getMail() {
        return mail;
    }

    public void setMail(String username) {
        this.mail = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}