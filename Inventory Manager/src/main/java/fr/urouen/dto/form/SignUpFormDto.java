package fr.urouen.dto.form;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class SignUpFormDto {
    @Pattern(regexp = "^[a-zA-ZÀ-ž\\s-]{1,30}$", message = "Le prénom doit contenir entre 1 et 30 caractères, composé uniquement de lettres, d'espaces ou de trait d'union")
    @NotBlank(message = "Le prénom est un champ obligatoire")
    private String firstName;

    @Pattern(regexp = "^[a-zA-ZÀ-ž\\s-]{1,30}$", message = "Le nom doit contenir entre 1 et 30 caractères, composé uniquement de lettres, d'espaces ou de trait d'union")
    @NotBlank(message = "Le nom est un champ obligatoire")
    private String lastName;

    @Pattern(regexp = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", message = "Le format de l'adresse email n'est pas valide")
    @NotBlank(message = "L'adresse email est un champ obligatoire")
    private String email;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_-,.!?]).{8,500}$", message = "Le mot de passe doit contenir au minimum 8 caractères dont une majuscule, une minuscule et un chiffre")
    @NotBlank(message = "Le mot de passe est un champ obligatoire")
    private String password;

    @NotBlank(message = "La confirmation du mot de passe est un champ obligatoire")
    private String confirmPassword;

    @JsonIgnore
    @AssertTrue(message = "La confirmation du mot de passe doit être identique au mot de passe")
    public boolean isValidConfirmPassword() {
        return password != null && confirmPassword != null && password.equals(confirmPassword);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}