package fr.urouen.dto.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class EditProfileFormDto {
    @Pattern(regexp = "^[a-zA-ZÀ-ž\\s-]{1,30}$", message = "Le prénom doit contenir entre 1 et 30 caractères, composé uniquement de lettres, d'espaces ou de trait d'union")
    @NotBlank(message = "Le prénom est un champ obligatoire")
    private String firstName;
    
    @Pattern(regexp = "^[a-zA-ZÀ-ž\\s-]{1,30}$", message = "Le prénom doit contenir entre 1 et 30 caractères, composé uniquement de lettres, d'espaces ou de trait d'union")
    @NotBlank(message = "Le nom est un champ obligatoire")
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}