package fr.urouen.dto.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

public class InventoryFormDto {
    @NotBlank(message = "Le nom est un champ obligatoire")
    @Size(min = 1, max = 30, message = "Le nom doit contenir entre 1 et 30 caractères")
    private String name;

    @Size(min = 0, max = 150, message = "La description doit contenir au maximum 150 caractères")
    private String description;
    private Map<Long, Long> products;

    @NotNull
    private Boolean closed;

    public InventoryFormDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<Long, Long> getProducts() {
        return products;
    }

    public void setProducts(Map<Long, Long> products) {
        this.products = products;
    }

    public Boolean isClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }
}