package fr.urouen.dto.form;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class EditPasswordFormDto {
    @NotBlank(message = "Le mot de passe actuel est un champ obligatoire")
    private String currentPassword;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,500}$", message = "Le mot de passe doit contenir au minimum 8 caractères dont une majuscule, une minuscule et un chiffre")
    @NotBlank(message = "Le mot de passe est un champ obligatoire")
    private String password;

    @NotBlank(message = "La confirmation du mot de passe est un champ obligatoire")
    private String confirmPassword;

    public EditPasswordFormDto() {
    }

    @JsonIgnore
    @AssertTrue(message = "La confirmation du mot de passe doit être identique au mot de passe")
    public boolean isValidConfirmPassword() {
        return password != null && confirmPassword != null && password.equals(confirmPassword);
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
