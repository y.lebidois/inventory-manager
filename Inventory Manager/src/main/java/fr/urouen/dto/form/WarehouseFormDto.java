package fr.urouen.dto.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Map;

public class WarehouseFormDto {
    @NotBlank(message = "Le nom est un champ obligatoire")
    @Size(min = 1, max = 30, message = "Le nom doit contenir entre 1 et 30 caractères")
    private String name;

    private Map<String, String> permissions;

    public WarehouseFormDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Map<String, String> permissions) {
        this.permissions = permissions;
    }
}
