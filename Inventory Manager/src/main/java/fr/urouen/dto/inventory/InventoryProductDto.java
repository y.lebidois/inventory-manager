package fr.urouen.dto.inventory;

import fr.urouen.dto.product.ProductDto;

public class InventoryProductDto {
    private ProductDto product;
    private Long quantity;

    public InventoryProductDto() {
    }

    public InventoryProductDto(ProductDto product, Long quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ProductDto getProduct() {
        return product;
    }

    public void setProduct(ProductDto product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}