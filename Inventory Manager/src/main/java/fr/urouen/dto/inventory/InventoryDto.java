package fr.urouen.dto.inventory;

import fr.urouen.dto.user.UserDto;

import java.time.LocalDateTime;
import java.util.List;

public class InventoryDto {
    private Long id;
    private String name;
    private String description;
    private UserDto author;
    private LocalDateTime date;
    private Boolean closed;
    private Boolean outOfStock;
    private List<InventoryProductDto> products;

    public InventoryDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserDto getAuthor() {
        return author;
    }

    public void setAuthor(UserDto author) {
        this.author = author;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public Boolean getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(Boolean outOfStock) {
        this.outOfStock = outOfStock;
    }

    public List<InventoryProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<InventoryProductDto> products) {
        this.products = products;
    }
}