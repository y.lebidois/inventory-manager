package fr.urouen.service;

import fr.urouen.dto.form.WarehouseFormDto;
import fr.urouen.dto.warehouse.WarehouseDto;
import fr.urouen.dto.warehouse.WarehousePermissionDto;
import fr.urouen.dto.warehouse.WarehouseStatisticsDto;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface WarehouseService {
    List<WarehouseDto> getMyWarehouses();
    WarehouseStatisticsDto getStatistics(Long id) throws EntityNotFoundException;
    List<WarehousePermissionDto> getPermissions(Long id) throws EntityNotFoundException;
    WarehouseDto create(WarehouseFormDto warehouse);
    WarehousePermissionDto createPermission(Long id, WarehousePermissionDto permission) throws EntityNotFoundException, EntityExistsException;
    WarehousePermissionDto updatePermission(Long id, WarehousePermissionDto permission) throws EntityNotFoundException;
    void deletePermission(Long id, WarehousePermissionDto permission) throws EntityNotFoundException;
}
