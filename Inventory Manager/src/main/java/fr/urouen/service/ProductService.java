package fr.urouen.service;

import fr.urouen.dto.form.ProductFormDto;
import fr.urouen.dto.product.ProductDto;
import org.springframework.security.access.AccessDeniedException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface ProductService {
    List<ProductDto> findByWarehouseId(Long id) throws EntityNotFoundException;
    ProductDto create(ProductFormDto product, Long warehouseId) throws EntityNotFoundException, EntityExistsException;
    ProductDto update(Long id, ProductFormDto product) throws EntityNotFoundException, EntityExistsException, AccessDeniedException;
    void delete(Long id) throws EntityNotFoundException, AccessDeniedException;
}