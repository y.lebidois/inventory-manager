package fr.urouen.service;

import fr.urouen.dto.form.InventoryFormDto;
import fr.urouen.dto.inventory.InventoryDto;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface InventoryService {
    InventoryDto findById(Long id) throws EntityNotFoundException;
    List<InventoryDto> findMyInventoriesByWarehouseId(Long warehouseId) throws EntityNotFoundException;
    List<InventoryDto> findByWarehouseId(Long warehouseId) throws EntityNotFoundException;
    List<InventoryDto> findLatestByWarehouseId(Long warehouseId) throws EntityNotFoundException;
    InventoryDto findInventoryProductsById(Long id) throws EntityNotFoundException;
    InventoryDto create(InventoryFormDto inventory, Long warehouseId) throws EntityNotFoundException;
    InventoryDto update(Long id, InventoryFormDto inventory) throws EntityNotFoundException, IllegalArgumentException;
    void delete(Long id) throws EntityNotFoundException, IllegalArgumentException;
}