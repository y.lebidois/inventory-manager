package fr.urouen.service;

import fr.urouen.dto.form.EditPasswordFormDto;
import fr.urouen.dto.form.EditProfileFormDto;
import fr.urouen.dto.form.SignUpFormDto;
import fr.urouen.dto.user.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public interface UserService extends UserDetailsService {
    UserDto findMyself();
    UserDto findByEmail(String email) throws EntityNotFoundException;
    UserDto updateInformation(EditProfileFormDto profile);
    UserDto updatePassword(EditPasswordFormDto passwords) throws IllegalArgumentException;
    UserDto create(SignUpFormDto form) throws EntityExistsException;
}