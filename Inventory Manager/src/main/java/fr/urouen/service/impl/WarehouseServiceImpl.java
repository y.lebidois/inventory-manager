package fr.urouen.service.impl;

import fr.urouen.dto.form.WarehouseFormDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.dto.warehouse.WarehouseDto;
import fr.urouen.dto.warehouse.WarehousePermissionDto;
import fr.urouen.dto.warehouse.WarehouseStatisticsAdminDto;
import fr.urouen.dto.warehouse.WarehouseStatisticsDto;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.UserRepository;
import fr.urouen.repository.WarehouseRepository;
import fr.urouen.service.WarehouseService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class WarehouseServiceImpl implements WarehouseService {
    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<WarehouseDto> getMyWarehouses() {
        List<WarehouseEntity> entities = warehouseRepository.findAccessiblesWarehouses(getAuthenticatedUser().getId());

        List<WarehouseDto> warehouses = new ArrayList<>();
        entities.forEach(entity -> {
            WarehouseDto warehouse = mapper.map(entity, WarehouseDto.class);
            WarehouseEntity.UserPermission permission = entity.getUserPermissions().get(getAuthenticatedUser());

            boolean isAdmin = entity.getCreator().equals(getAuthenticatedUser()) ||
                    (permission == WarehouseEntity.UserPermission.ADMIN);

            warehouse.setAdmin(isAdmin);

            warehouses.add(warehouse);
        });

        return warehouses;
    }

    @Override
    @PreAuthorize("hasWarehouseUserPermission(#id)")
    public WarehouseStatisticsDto getStatistics(Long id) throws EntityNotFoundException {
        Optional<WarehouseEntity> optional = warehouseRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        WarehouseEntity warehouse = optional.get();
        WarehouseEntity.UserPermission permission = warehouse.getUserPermissions().get(getAuthenticatedUser());

        LocalDateTime lastClosedDate = warehouse
                .getInventories()
                .stream()
                .filter(i -> i.isClosed())
                .sorted(Comparator.comparing(i -> i.getDate(), Comparator.nullsLast(Comparator.reverseOrder())))
                .findFirst()
                .map(i -> i.getDate())
                .orElse(null);

        int pendingInventoriesCount = (int) warehouse
                .getInventories()
                .stream()
                .filter(i -> i.getAuthor().equals(getAuthenticatedUser()) && !i.isClosed())
                .count();

        int productsCount = (int) warehouse
                .getProducts()
                .stream()
                .filter(w -> w.isEnabled())
                .count();

        int myInventoriesCount = (int) warehouse
                .getInventories()
                .stream()
                .filter(i -> i.getAuthor().equals(getAuthenticatedUser()))
                .count();

        if (warehouse.getCreator().getId().equals(getAuthenticatedUser().getId()) ||
                permission != null && permission.equals(WarehouseEntity.UserPermission.ADMIN)) {
            WarehouseStatisticsAdminDto dto = new WarehouseStatisticsAdminDto();

            if (warehouse.getCreator().equals(getAuthenticatedUser())) {
                dto.setStatus("Propriétaire");
            } else {
                dto.setStatus("Administrateur");
            }

            dto.setUsersCount(warehouse.getUserPermissions().size());
            dto.setInventoriesClosedCount((int) warehouse
                    .getInventories()
                    .stream()
                    .filter(i -> i.isClosed())
                    .count());

            dto.setOutOfStockCount((int) warehouse
                    .getProducts()
                    .stream()
                    .filter(p -> p.getCurrentQuantity() != null && p.getCurrentQuantity() == 0)
                    .count());

            dto.setLastClosedDate(lastClosedDate);
            dto.setPendingInventoriesCount(pendingInventoriesCount);
            dto.setProductsCount(productsCount);
            dto.setMyInventoriesCount(myInventoriesCount);

            return dto;
        }

        WarehouseStatisticsDto dto = new WarehouseStatisticsDto();
        dto.setLastClosedDate(lastClosedDate);
        dto.setPendingInventoriesCount(pendingInventoriesCount);
        dto.setProductsCount(productsCount);
        dto.setMyInventoriesCount(myInventoriesCount);

        return dto;
    }

    @Override
    @PreAuthorize("hasWarehouseAdminPermission(#id)")
    public List<WarehousePermissionDto> getPermissions(Long id) throws EntityNotFoundException {
        Optional<WarehouseEntity> optional = warehouseRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        Map<UserEntity, WarehouseEntity.UserPermission> permissions = optional.get().getUserPermissions();

        List<WarehousePermissionDto> dtos = new ArrayList<>();
        permissions.forEach((u, p) -> {
            if (u.equals(getAuthenticatedUser())) {
                return;
            }

            WarehousePermissionDto dto = new WarehousePermissionDto();
            dto.setUser(mapper.map(u, UserDto.class));
            dto.setAdmin(p == WarehouseEntity.UserPermission.ADMIN);

            dtos.add(dto);
        });

        return dtos;
    }

    @Override
    public WarehouseDto create(@Valid WarehouseFormDto warehouse) {
        WarehouseEntity entity = new WarehouseEntity();
        entity.setCreator(getAuthenticatedUser());
        entity.setName(warehouse.getName());

        Map<UserEntity, WarehouseEntity.UserPermission> permissions = new HashMap<>();

        // Setting up users permissions of the warehouse
        for (Map.Entry<String, String> entry : warehouse.getPermissions().entrySet()) {
            Optional<UserEntity> user = userRepository.findByEmail(entry.getKey());

            if (user.isPresent() && !user.get().equals(getAuthenticatedUser())) {
                permissions.put(user.get(), WarehouseEntity.UserPermission.getUserPermission(entry.getValue()));
            }
        }

        entity.setUserPermissions(permissions);

        return mapper.map(warehouseRepository.save(entity), WarehouseDto.class);
    }

    @Override
    @PreAuthorize("hasWarehouseAdminPermission(#id)")
    public WarehousePermissionDto createPermission(Long id, @Valid WarehousePermissionDto permission) throws EntityNotFoundException, EntityExistsException {
        Optional<WarehouseEntity> optional = warehouseRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        WarehouseEntity warehouse = optional.get();

        Optional<UserEntity> user = userRepository.findByEmail(permission.getUser().getEmail());

        if (!user.isPresent()) {
            throw new EntityNotFoundException("L'utilisateur n'existe pas");
        }

        if (user.get().equals(warehouse.getCreator())) {
            throw new EntityExistsException("Cet utilisateur est déjà le propriétaire de cet entrepôt");
        }

        warehouse.getUserPermissions().put(user.get(), permission.isAdmin() ? WarehouseEntity.UserPermission.ADMIN : WarehouseEntity.UserPermission.USER);

        warehouseRepository.save(warehouse);

        return permission;
    }

    @Override
    @PreAuthorize("hasWarehouseAdminPermission(#id)")
    public WarehousePermissionDto updatePermission(Long id, @Valid WarehousePermissionDto permission) throws EntityNotFoundException {
        Optional<WarehouseEntity> optional = warehouseRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        WarehouseEntity warehouse = optional.get();

        Optional<UserEntity> user = userRepository.findByEmail(permission.getUser().getEmail());

        if (!user.isPresent()) {
            throw new EntityNotFoundException("L'utilisateur n'existe pas");
        }

        if (warehouse.getUserPermissions().get(user.get()) == null) {
            throw new EntityNotFoundException("Cette permission n'existe pas");
        }

        warehouse.getUserPermissions().put(user.get(), permission.isAdmin() ? WarehouseEntity.UserPermission.ADMIN : WarehouseEntity.UserPermission.USER);

        warehouseRepository.save(warehouse);

        return permission;
    }

    @Override
    @PreAuthorize("hasWarehouseAdminPermission(#id)")
    public void deletePermission(Long id, @Valid WarehousePermissionDto permission) throws EntityNotFoundException {
        Optional<WarehouseEntity> optional = warehouseRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        WarehouseEntity warehouse = optional.get();

        Optional<UserEntity> user = userRepository.findByEmail(permission.getUser().getEmail());

        if (!user.isPresent()) {
            throw new EntityNotFoundException("L'utilisateur n'existe pas");
        }

        if (warehouse.getUserPermissions().get(user.get()) == null) {
            throw new EntityNotFoundException("Cette permission n'existe pas");
        }

        warehouse.getUserPermissions().remove(user.get());

        warehouseRepository.save(warehouse);
    }

    private UserEntity getAuthenticatedUser() {
        return (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
