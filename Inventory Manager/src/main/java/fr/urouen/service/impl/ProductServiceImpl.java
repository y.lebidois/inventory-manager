package fr.urouen.service.impl;

import fr.urouen.dto.form.ProductFormDto;
import fr.urouen.dto.product.ProductDto;
import fr.urouen.entity.ProductEntity;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.ProductRepository;
import fr.urouen.repository.WarehouseRepository;
import fr.urouen.service.ProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    @PreAuthorize("hasWarehouseUserPermission(#warehouseId)")
    public List<ProductDto> findByWarehouseId(Long warehouseId) throws EntityNotFoundException {
        Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

        if (!warehouse.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        List<ProductEntity> products = productRepository.findByWarehouseIdAndEnabledIsTrueOrderByName(warehouseId);

        Type type = new TypeToken<List<ProductDto>>() {
        }.getType();

        return mapper.map(products, type);
    }

    @Override
    @PreAuthorize("hasWarehouseAdminPermission(#warehouseId)")
    public ProductDto create(@Valid ProductFormDto product, Long warehouseId) throws EntityNotFoundException, EntityExistsException {
        Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

        if (!warehouse.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        boolean hasProduct = productRepository
                .findByWarehouseIdAndEnabledIsTrueOrderByName(warehouseId)
                .stream()
                .anyMatch(p -> p.getName().equals(product.getName()));

        if (hasProduct) {
            throw new EntityExistsException("Ce nom de produit est déjà utilisé par un autre produit");
        }

        ProductEntity entity = new ProductEntity();
        entity.setName(product.getName());
        entity.setDescription(product.getDescription());
        entity.setWarehouse(warehouse.get());
        entity.setEnabled(true);

        productRepository.save(entity);

        return mapper.map(entity, ProductDto.class);
    }

    @Override
    public ProductDto update(Long id, @Valid ProductFormDto product) throws EntityNotFoundException, EntityExistsException, AccessDeniedException {
        Optional<ProductEntity> optional = productRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("Le produit n'existe pas");
        }

        ProductEntity entity = optional.get();

        boolean hasProduct = productRepository
                .findByWarehouseIdAndEnabledIsTrueOrderByName(entity.getWarehouse().getId())
                .stream()
                .anyMatch(p -> p.getId() != entity.getId() && p.getName().equals(product.getName()));

        if (hasProduct) {
            throw new EntityExistsException("Ce nom de produit est déjà utilisé par un autre produit");
        }

        WarehouseEntity.UserPermission permission = entity.getWarehouse().getUserPermissions().get(getAuthenticatedUser());

        if (!entity.getWarehouse().getCreator().equals(getAuthenticatedUser()) && permission != WarehouseEntity.UserPermission.ADMIN) {
            throw new AccessDeniedException("Accès refusé");
        }

        entity.setName(product.getName());
        entity.setDescription(product.getDescription());

        productRepository.save(entity);

        return mapper.map(entity, ProductDto.class);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException, AccessDeniedException {
        Optional<ProductEntity> optional = productRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("Le produit n'existe pas");
        }

        ProductEntity entity = optional.get();

        WarehouseEntity.UserPermission permission = entity.getWarehouse().getUserPermissions().get(getAuthenticatedUser());

        if (!entity.getWarehouse().getCreator().equals(getAuthenticatedUser()) && permission != WarehouseEntity.UserPermission.ADMIN) {
            throw new AccessDeniedException("Accès refusé");
        }

        entity.setEnabled(false);

        productRepository.save(entity);
    }

    private UserEntity getAuthenticatedUser() {
        return (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}