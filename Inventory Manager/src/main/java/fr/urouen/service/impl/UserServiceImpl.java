package fr.urouen.service.impl;

import fr.urouen.dto.form.EditPasswordFormDto;
import fr.urouen.dto.form.EditProfileFormDto;
import fr.urouen.dto.form.SignUpFormDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.entity.UserEntity;
import fr.urouen.repository.UserRepository;
import fr.urouen.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Problème d'authentification"));
    }

    @Override
    public UserDto findMyself() {
        return mapper.map(getAuthenticatedUser(), UserDto.class);
    }

    @Override
    public UserDto findByEmail(String email) throws EntityNotFoundException {
        Optional<UserEntity> optional = userRepository.findByEmail(email);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("Cet utilisateur n'existe pas");
        }

        return mapper.map(optional.get(), UserDto.class);
    }

    @Override
    public UserDto updateInformation(@Valid EditProfileFormDto profile) {
        UserEntity authenticatedUser = getAuthenticatedUser();

        authenticatedUser.setFirstName(profile.getFirstName());
        authenticatedUser.setLastName(profile.getLastName());

        userRepository.save(authenticatedUser);

        return mapper.map(authenticatedUser, UserDto.class);
    }

    @Override
    public UserDto updatePassword(@Valid EditPasswordFormDto passwords) throws IllegalArgumentException {
        UserEntity authenticatedUser = getAuthenticatedUser();

        if (!passwordEncoder.matches(passwords.getCurrentPassword(), authenticatedUser.getPassword())) {
            throw new IllegalArgumentException("Mot de passe incorrect");
        }

        authenticatedUser.setPassword(passwordEncoder.encode(passwords.getPassword()));

        userRepository.save(authenticatedUser);

        return mapper.map(authenticatedUser, UserDto.class);
    }

    @Override
    public UserDto create(@Valid SignUpFormDto form) throws EntityExistsException {
        if (userRepository.findByEmail(form.getEmail()).isPresent()) {
            throw new EntityExistsException("Cette adresse email est déjà utilisée par un autre utilisateur");
        }

        form.setPassword(passwordEncoder.encode(form.getPassword()));

        UserEntity user = mapper.map(form, UserEntity.class);

        userRepository.save(user);

        return mapper.map(user, UserDto.class);
    }

    private UserEntity getAuthenticatedUser() {
        UserEntity authenticatedUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByEmail(authenticatedUser.getEmail()).get();
    }
}