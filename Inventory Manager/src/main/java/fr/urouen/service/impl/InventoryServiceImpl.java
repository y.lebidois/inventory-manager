package fr.urouen.service.impl;

import fr.urouen.dto.form.InventoryFormDto;
import fr.urouen.dto.inventory.InventoryDto;
import fr.urouen.dto.inventory.InventoryProductDto;
import fr.urouen.dto.product.ProductDto;
import fr.urouen.entity.InventoryEntity;
import fr.urouen.entity.ProductEntity;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.InventoryRepository;
import fr.urouen.repository.ProductRepository;
import fr.urouen.repository.WarehouseRepository;
import fr.urouen.service.InventoryService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InventoryServiceImpl implements InventoryService {
    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    @PreAuthorize("hasInventoryReadPermission(#id)")
    public InventoryDto findById(Long id) throws EntityNotFoundException {
        Optional<InventoryEntity> optional = inventoryRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'inventaire n'existe pas");
        }

        return mapper.map(optional.get(), InventoryDto.class);
    }

    @Override
    @PreAuthorize("hasWarehouseUserPermission(#warehouseId)")
    public List<InventoryDto> findMyInventoriesByWarehouseId(Long warehouseId) throws EntityNotFoundException {
        Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

        if (!warehouse.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        List<InventoryDto> dtos = inventoryRepository.findByAuthorIdOrderByDateDesc(getAuthenticatedUser().getId())
                .stream()
                .filter(i -> i.getWarehouse().equals(warehouse.get()))
                .map(i -> mapper.map(i, InventoryDto.class))
                .collect(Collectors.toList());

        return dtos;
    }

    @Override
    @PreAuthorize("hasWarehouseAdminPermission(#warehouseId)")
    public List<InventoryDto> findByWarehouseId(Long warehouseId) throws EntityNotFoundException {
        Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

        if (!warehouse.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        List<InventoryEntity> entities = inventoryRepository.findByWarehouseIdAndClosedIsTrueOrderByDateDesc(warehouseId);

        Type type = new TypeToken<List<InventoryDto>>() {
        }.getType();

        return mapper.map(entities, type);
    }

    @Override
    @PreAuthorize("hasWarehouseUserPermission(#warehouseId)")
    public List<InventoryDto> findLatestByWarehouseId(Long warehouseId) throws EntityNotFoundException {
        Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

        if (!warehouse.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        List<InventoryEntity> entities = inventoryRepository.findTop5ByWarehouseIdAndClosedIsTrueOrderByDateDesc(warehouseId);

        Type type = new TypeToken<List<InventoryDto>>() {
        }.getType();

        return mapper.map(entities, type);
    }

    @Override
    @PreAuthorize("hasInventoryReadPermission(#id)")
    public InventoryDto findInventoryProductsById(Long id) throws EntityNotFoundException {
        Optional<InventoryEntity> optional = inventoryRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'inventaire n'existe pas");
        }

        InventoryEntity entity = optional.get();

        InventoryDto dto = mapper.map(entity, InventoryDto.class);
        ;

        List<InventoryProductDto> products = new ArrayList<>();
        for (Map.Entry<ProductEntity, Long> entry : entity.getProducts().entrySet()) {
            products.add(new InventoryProductDto(mapper.map(entry.getKey(), ProductDto.class), entry.getValue()));
        }
        dto.setProducts(products);

        return dto;
    }

    @Override
    @PreAuthorize("hasWarehouseUserPermission(#warehouseId)")
    public InventoryDto create(@Valid InventoryFormDto inventory, Long warehouseId) throws EntityNotFoundException {
        Optional<WarehouseEntity> warehouse = warehouseRepository.findById(warehouseId);

        if (!warehouse.isPresent()) {
            throw new EntityNotFoundException("L'entrepôt n'existe pas");
        }

        InventoryEntity entity = new InventoryEntity();
        entity.setName(inventory.getName());
        entity.setDescription(inventory.getDescription());
        entity.setAuthor(getAuthenticatedUser());
        entity.setDate(LocalDateTime.now());
        entity.setClosed(inventory.isClosed());

        Map<ProductEntity, Long> products = new HashMap<>();
        for (Map.Entry<Long, Long> entry : inventory.getProducts().entrySet()) {
            Optional<ProductEntity> optionalProduct = productRepository.findById(entry.getKey());

            if (!optionalProduct.isPresent()) {
                throw new EntityNotFoundException("Le produit n'existe pas");
            }

            ProductEntity product = optionalProduct.get();

            if (entity.isClosed()) {
                product.setCurrentQuantity(Math.max(0, entry.getValue()));
            }

            products.put(product, entry.getValue());

            productRepository.save(product);
        }
        entity.setProducts(products);
        entity.setWarehouse(warehouse.get());

        inventoryRepository.save(entity);

        return mapper.map(entity, InventoryDto.class);
    }

    @Override
    @PreAuthorize("hasInventoryWritePermission(#id)")
    public InventoryDto update(Long id, @Valid InventoryFormDto inventory) throws EntityNotFoundException, IllegalArgumentException {
        Optional<InventoryEntity> optional = inventoryRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'inventaire n'existe pas");
        }

        InventoryEntity entity = optional.get();

        if (entity.isClosed()) {
            throw new IllegalArgumentException("Un inventaire clôturé ne peut pas être modifié");
        }

        entity.setName(inventory.getName());
        entity.setDescription(inventory.getDescription());
        entity.setDate(LocalDateTime.now());
        entity.setClosed(inventory.isClosed());

        Map<ProductEntity, Long> products = new HashMap<>();
        for (Map.Entry<Long, Long> entry : inventory.getProducts().entrySet()) {
            Optional<ProductEntity> optionalProduct = productRepository.findById(entry.getKey());

            if (!optionalProduct.isPresent()) {
                throw new EntityNotFoundException("Le produit n'existe pas");
            }

            ProductEntity product = optionalProduct.get();

            if (entity.isClosed()) {
                product.setCurrentQuantity(Math.max(0, entry.getValue()));
            }

            products.put(product, entry.getValue());

            productRepository.save(product);
        }
        entity.setProducts(products);

        inventoryRepository.save(entity);

        return mapper.map(entity, InventoryDto.class);
    }

    @Override
    @PreAuthorize("hasInventoryWritePermission(#id)")
    public void delete(Long id) throws EntityNotFoundException, IllegalArgumentException {
        Optional<InventoryEntity> optional = inventoryRepository.findById(id);

        if (!optional.isPresent()) {
            throw new EntityNotFoundException("L'inventaire n'existe pas");
        }

        InventoryEntity entity = optional.get();

        if (entity.isClosed()) {
            throw new IllegalArgumentException("Un inventaire cloturé ne peut pas être supprimé");
        }

        inventoryRepository.delete(entity);
    }

    private UserEntity getAuthenticatedUser() {
        return (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}