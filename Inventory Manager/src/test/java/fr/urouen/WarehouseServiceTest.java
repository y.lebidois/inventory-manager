package fr.urouen;

import fr.urouen.dto.form.ProductFormDto;
import fr.urouen.dto.form.WarehouseFormDto;
import fr.urouen.dto.product.ProductDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.dto.warehouse.WarehouseDto;
import fr.urouen.dto.warehouse.WarehousePermissionDto;
import fr.urouen.dto.warehouse.WarehouseStatisticsDto;
import fr.urouen.entity.ProductEntity;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.ProductRepository;
import fr.urouen.repository.UserRepository;
import fr.urouen.repository.WarehouseRepository;
import fr.urouen.service.ProductService;
import fr.urouen.service.WarehouseService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WarehouseServiceTest {
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private WarehouseRepository warehouseRepository;

    @MockBean
    private SecurityContextHolder securityContextHolder;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private ModelMapper mapper;

    private List<WarehouseEntity> warehouseEntities;
    private UserEntity userEntity;
    private UserDto userDto;

    @Before
    public void setUpWarehouse() {
        userEntity = new UserEntity("Jean", "Dupont", "jean.dupont@mail.com", "P4Ssw0rD");
        userDto = mapper.map(userEntity, UserDto.class);

        when(userRepository.findByEmail("jean.dupont@mail.com")).thenReturn(Optional.of(userEntity));
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        securityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userEntity);


        WarehouseEntity w = new WarehouseEntity();
        w.setId(new Long(1));
        w.setCreator(userEntity);
        w.setName("Entrepot");
        w.setUserPermissions(new HashMap<>());
        w.setInventories(new ArrayList<>());
        w.setProducts(new ArrayList<>());

        warehouseEntities = new ArrayList<>();
        warehouseEntities.add(w);

        Mockito.when(warehouseRepository.findAccessiblesWarehouses(new Long(1))).thenReturn(warehouseEntities);
    }

    /* -------- True values -------- */

    @Test
    public void testFindByWarehouseId() throws Exception {
        List<WarehouseDto> lst = warehouseService.getMyWarehouses();

        assertEquals(lst.size(), warehouseEntities.size());
    }

    @Test
    public void testGetStatistics() throws Exception {
        WarehouseStatisticsDto w = warehouseService.getStatistics(new Long(1));

        assertEquals(w.getMyInventoriesCount(), 0);
        assertEquals(w.getPendingInventoriesCount(), 0);
        assertEquals(w.getProductsCount(), 0);
    }

    @Test
    public void testCreate() throws Exception {
        Map<String, String> permission = new HashMap<>();
        permission.put("jean.dupont@mail.com", "ADMIN");

        WarehouseFormDto w = new WarehouseFormDto();
        w.setName("Entrepot");
        w.setPermissions(permission);

        WarehouseDto warehouse = warehouseService.create(w);

        assertEquals(w.getName(), warehouse.getName());
    }

    @Test
    public void testCreatePermission() throws Exception {
        WarehousePermissionDto w = new WarehousePermissionDto();
        w.setAdmin(true);
        w.setUser(userDto);

        WarehousePermissionDto permission = warehouseService.createPermission(new Long(2), w);

        assertEquals(permission.getUser(), userDto);
    }

    @Test
    public void testUpdatePermission() throws Exception {
        WarehousePermissionDto w = new WarehousePermissionDto();
        w.setAdmin(true);
        w.setUser(userDto);

        WarehousePermissionDto permissionImport = new WarehousePermissionDto();
        permissionImport.setAdmin(true);
        permissionImport.setUser(userDto);

        warehouseService.createPermission(new Long(1), w);

        WarehousePermissionDto permission = warehouseService.updatePermission(new Long(1), permissionImport);

        assertEquals(permission.getUser(), userDto);
    }

    @Test
    public void testDeletePermission() throws Exception {
        WarehousePermissionDto w = new WarehousePermissionDto();
        w.setAdmin(true);
        w.setUser(userDto);

        warehouseService.createPermission(new Long(1), w);
        warehouseService.deletePermission(new Long(1), w);
    }

    @Test
    public void testGetPermission() throws Exception {
        WarehousePermissionDto w = new WarehousePermissionDto();
        w.setAdmin(true);
        w.setUser(userDto);

        warehouseService.createPermission(new Long(1), w);

        List<WarehousePermissionDto> lst = warehouseService.getPermissions(new Long(1));

        assertEquals(lst.size(), 1);
    }

    /* -------- False values -------- */

    /* -------- Exception values -------- */

}
