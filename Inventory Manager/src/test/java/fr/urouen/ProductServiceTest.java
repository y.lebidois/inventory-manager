package fr.urouen;

import fr.urouen.dto.form.ProductFormDto;
import fr.urouen.dto.form.SignUpFormDto;
import fr.urouen.dto.product.ProductDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.entity.ProductEntity;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.ProductRepository;
import fr.urouen.repository.UserRepository;
import fr.urouen.service.ProductService;
import fr.urouen.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @MockBean
    private SecurityContextHolder securityContextHolder;

    @Autowired
    private ModelMapper mapper;

    private ProductDto productDto;
    private List<ProductEntity> productEntities;
    private UserDto userDto;
    private UserEntity userEntity;
    private WarehouseEntity warehouseEntity;

    @Before
    public void setUpProduct() {
        userEntity = new UserEntity("Jean", "Dupont", "jean.dupont@mail.com", "P4Ssw0rD");
        userDto = mapper.map(userEntity, UserDto.class);

        when(userRepository.findByEmail("jean.dupont@mail.com")).thenReturn(Optional.of(userEntity));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        securityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userEntity);

        warehouseEntity = new WarehouseEntity();
        warehouseEntity.setId(new Long(1));

        ProductEntity productEntity = new ProductEntity(new Long(1), "Produit #1", "Description", new Long(223), true, warehouseEntity);
        productEntities = new ArrayList<>();
        productEntities.add(productEntity);

       // productDto = mapper.map(productEntity, ProductDto.class);

        Mockito.when(productRepository.findByWarehouseIdAndEnabledIsTrueOrderByName(new Long(1))).thenReturn(productEntities);
    }

    /* -------- True values -------- */

    @Test
    public void testFindByWarehouseId() throws Exception {
        List<ProductDto> lst = productService.findByWarehouseId(new Long(1));

        ProductDto firstProduct = mapper.map(productEntities.get(0), ProductDto.class);

        assertEquals(lst.get(0).getId(), firstProduct.getId());
    }

    @Test
    public void testProductCreate() throws Exception {
        ProductFormDto productForm = new ProductFormDto();
        productForm.setName("Produit #2");
        productForm.setDescription("Description");

        ProductDto p = productService.create(productForm, warehouseEntity.getId());

        assertEquals(p.getName(), productForm.getName());
    }

    @Test
    public void testProductUpdate() throws Exception {
        ProductFormDto productForm = new ProductFormDto();
        productForm.setName("Produit #4");
        productForm.setDescription("Description ...");

        ProductDto p = productService.update(new Long(1), productForm);

        assertEquals(p.getName(), productForm.getName());
        assertEquals(p.getDescription(), productForm.getDescription());
    }

    @Test
    public void testProductDelete() throws Exception {
        productService.delete(new Long(1));

        assertNotEquals(productEntities.size(), productService.findByWarehouseId(new Long(1)).size());
    }

    /* -------- False values -------- */

    /* -------- Exception values -------- */

    @Test(expected = Exception.class)
    public void testFindByWarehouseIdException() throws Exception {
        productService.findByWarehouseId(new Long(8));
    }

    @Test(expected = Exception.class)
    public void testProductCreateException() throws Exception {
        ProductFormDto productForm = new ProductFormDto();
        productForm.setName("");
        productForm.setDescription("");

        ProductDto p = productService.create(productForm, warehouseEntity.getId());

        assertEquals(p.getName(), productForm.getName());
    }

    @Test(expected = Exception.class)
    public void testProductUpdateExpetion() throws Exception {
        ProductFormDto productForm = new ProductFormDto();
        productForm.setName("Produit #3");
        productForm.setDescription("Description ...");

        ProductDto p = productService.update(new Long(9), productForm);
    }

    @Test(expected = Exception.class)
    public void testProductDeleteException() throws Exception {
        productService.delete(new Long(6));
    }
}
