package fr.urouen;

import fr.urouen.dto.form.EditPasswordFormDto;
import fr.urouen.dto.form.EditProfileFormDto;
import fr.urouen.dto.form.SignUpFormDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.entity.UserEntity;
import fr.urouen.repository.UserRepository;
import fr.urouen.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @MockBean
    private SecurityContextHolder securityContextHolder;

    @Autowired
    private ModelMapper mapper;

    private UserDto userDto;
    private UserEntity userEntity;

    @Before
    public void setUpUser() {
        userEntity = new UserEntity("Jean", "Dupont", "jean.dupont@mail.com", "P4Ssw0rD");
        userDto = mapper.map(userEntity, UserDto.class);

        when(userRepository.findByEmail("jean.dupont@mail.com")).thenReturn(Optional.of(userEntity));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        securityContextHolder.setContext(securityContext);
        when(securityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userEntity);
    }

    /* -------- True values -------- */

    @Test
    public void testUserFindMySelf() {
        assertEquals(userService.findMyself().getFirstName(), userDto.getFirstName());
        assertEquals(userService.findMyself().getLastName(), userDto.getLastName());
        assertEquals(userService.findMyself().getEmail(), userDto.getEmail());
    }

    @Test
    public void testUserFindByEmailEquals() throws Exception {
        assertEquals(userService.findByEmail("jean.dupont@mail.com").getFirstName(), userDto.getFirstName());
        assertEquals(userService.findByEmail("jean.dupont@mail.com").getLastName(), userDto.getLastName());
        assertEquals(userService.findByEmail("jean.dupont@mail.com").getEmail(), userDto.getEmail());
    }

    @Test
    public void testUserUpdateInformation() throws Exception {
        EditProfileFormDto editProfileFormDto = new EditProfileFormDto();
        editProfileFormDto.setFirstName("Prénomp");
        editProfileFormDto.setLastName("Nom");

        UserDto u = userService.updateInformation(editProfileFormDto);

        assertEquals(u.getFirstName(), "Prénomp");
        assertEquals(u.getLastName(), "Nom");
    }

    @Test
    public void testUserUpdatePassword() throws Exception {
        EditPasswordFormDto editPasswordFormDto = new EditPasswordFormDto();
        editPasswordFormDto.setCurrentPassword("P4Ssw0rD");
        editPasswordFormDto.setPassword("P4Ssw0rD_645");
        editPasswordFormDto.setConfirmPassword("P4Ssw0rD_645");

        UserDto u = userService.updatePassword(editPasswordFormDto);
    }

    @Test
    public void testUserCreate() throws Exception {
        SignUpFormDto signUp = new SignUpFormDto();
        signUp.setFirstName("David");
        signUp.setLastName("Dupont");
        signUp.setEmail("david.dupont@mail.com");
        signUp.setPassword("P4Ssw0rD");
        signUp.setConfirmPassword("P4Ssw0rD");

        UserDto u = userService.create(signUp);

        assertEquals(u.getFirstName(), signUp.getFirstName());
        assertEquals(u.getLastName(), signUp.getLastName());
        assertEquals(u.getEmail(), signUp.getEmail());
    }

    /* -------- False values -------- */


    /* -------- Exception values -------- */

    @Test(expected = Exception.class)
    public void testUserCreateExceptionAlreadyExist() throws Exception {
        SignUpFormDto signUp = new SignUpFormDto();
        signUp.setFirstName("Jean");
        signUp.setLastName("Dupont");
        signUp.setEmail("jean.dupont@mail.com");
        signUp.setPassword("P4Ssw0rD");
        signUp.setConfirmPassword("P4Ssw0rD");

        UserDto u = userService.create(signUp);
    }

    @Test(expected = Exception.class)
    public void testUserCreateExceptionPasswordNotEquals() throws AssertionError, Exception {
        SignUpFormDto signUp = new SignUpFormDto();
        signUp.setFirstName("David");
        signUp.setLastName("Dupont");
        signUp.setEmail("david.dupont@mail.com");
        signUp.setPassword("P4Ssw0rD");
        signUp.setConfirmPassword("P4Ssw0rD_268");

        UserDto u = userService.create(signUp);
    }

    @Test(expected = Exception.class)
    public void testUserFindByEmailExceptionNotFound() throws Exception {
        assertEquals(userService.findByEmail("jean.dupont.4@mail.com").getFirstName(),"Jean");
    }
}
