package fr.urouen;

import fr.urouen.dto.form.InventoryFormDto;
import fr.urouen.dto.form.ProductFormDto;
import fr.urouen.dto.inventory.InventoryDto;
import fr.urouen.dto.product.ProductDto;
import fr.urouen.dto.user.UserDto;
import fr.urouen.entity.InventoryEntity;
import fr.urouen.entity.ProductEntity;
import fr.urouen.entity.UserEntity;
import fr.urouen.entity.WarehouseEntity;
import fr.urouen.repository.InventoryRepository;
import fr.urouen.repository.ProductRepository;
import fr.urouen.repository.UserRepository;
import fr.urouen.service.InventoryService;
import fr.urouen.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryServiceTest {
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private InventoryRepository inventoryRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private ModelMapper mapper;

    private ProductDto productDto;
    private UserDto userDto;
    private InventoryDto inventoryDto;

    private List<ProductEntity> productEntities;
    private List<InventoryService> inventoryEntities;

    private Map<ProductEntity, Long> productEntityLongMap;

    private UserEntity userEntity;
    private WarehouseEntity warehouseEntity;
    private InventoryEntity inventoryEntity;

    @Before
    public void setUpInventory() {
        userEntity = new UserEntity("Jean", "Dupont", "jean.dupont@mail.com", "P4Ssw0rD");
        userDto = mapper.map(userEntity, UserDto.class);

        when(userRepository.findByEmail("jean.dupont@mail.com")).thenReturn(Optional.of(userEntity));
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(userEntity);

        warehouseEntity = new WarehouseEntity();
        warehouseEntity.setId(new Long(1));

        ProductEntity productEntity = new ProductEntity(new Long(1), "Produit #1", "Description", new Long(223), true, warehouseEntity);
        productEntities = new ArrayList<>();
        productEntities.add(productEntity);

        productEntityLongMap = new HashMap<>();
        productEntityLongMap.put(productEntity, new Long(1));

        inventoryEntity = new InventoryEntity();
        inventoryEntity.setId(new Long(1));
        inventoryEntity.setClosed(false);
        inventoryEntity.setDate(LocalDateTime.now());
        inventoryEntity.setDescription("Inventaire ...");
        inventoryEntity.setName("Inventaire #1");
        inventoryEntity.setAuthor(userEntity);
        inventoryEntity.setProducts(productEntityLongMap);
        inventoryEntity.setWarehouse(warehouseEntity);

        List<InventoryEntity> inventoryEntities = new ArrayList<>();
        inventoryEntities.add(inventoryEntity);

        Mockito.when(inventoryRepository.findByAuthorIdOrderByDateDesc(new Long(1))).thenReturn(inventoryEntities);
        Mockito.when(inventoryRepository.findByWarehouseIdAndClosedIsTrueOrderByDateDesc(new Long(1))).thenReturn(inventoryEntities);
        Mockito.when(inventoryRepository.findTop5ByWarehouseIdAndClosedIsTrueOrderByDateDesc(new Long(1))).thenReturn(inventoryEntities);
    }

    /* -------- True values -------- */

    /*
    InventoryDto update(Long id, InventoryFormDto inventory) throws Exception;
     */

    /*
    @Test
    public void testFindById() throws Exception {
        InventoryDto inventoryDto = inventoryService.findById(new Long(1));

        assertEquals(inventoryDto.getId(), inventoryEntity.getId());
        assertEquals(inventoryDto.getAuthor(), inventoryEntity.getAuthor());
        assertEquals(inventoryDto.getDate(), inventoryEntity.getDate());
        assertEquals(inventoryDto.getDescription(), inventoryEntity.getDescription());
        assertEquals(inventoryDto.getName(), inventoryEntity.getName());
        assertEquals(inventoryDto.isClosed(), inventoryEntity.isClosed());
        assertEquals(inventoryDto.isOutOfStock(), inventoryEntity.isOutOfStock());
    }*/

    @Test
    public void testFindMyInventoriesByWarehouseId() throws Exception {
        List<InventoryDto> inventoryDto = inventoryService.findMyInventoriesByWarehouseId(new Long(1));

        assertEquals(inventoryDto.size(), inventoryEntities.size());
    }

    @Test
    public void testFindByWarehouseIdById() throws Exception {
        List<InventoryDto> inventoryDto = inventoryService.findByWarehouseId(new Long(1));

        assertEquals(inventoryDto.size(), inventoryEntities.size());
    }

    /*
    @Test
    public void testFindLatestByWarehouseIdById() throws Exception {
        Map<ProductDto, Long> inventoryDto = inventoryService.findInventoryProductsById(new Long(1));

        assertEquals(inventoryDto.size(), productEntityLongMap.size());
    }*/

    @Test
    public void testCreate() throws Exception {
        Map<Long, Long> m = new HashMap<>();
        m.put(new Long(1), new Long(1));

        InventoryFormDto inventoryFormDto = new InventoryFormDto();
        inventoryFormDto.setClosed(false);
        inventoryFormDto.setDescription("Description ...");
        inventoryFormDto.setName("Inventaire #2");
        inventoryFormDto.setProducts(m);

        InventoryDto inventoryDto = inventoryService.create(inventoryFormDto, new Long(1));

        assertEquals(inventoryDto.getName(), inventoryFormDto.getName());
        assertEquals(inventoryDto.getDescription(), inventoryFormDto.getDescription());
    }

    @Test
    public void testUpdate() throws Exception {
        Map<Long, Long> m = new HashMap<>();
        m.put(new Long(1), new Long(1));

        InventoryFormDto inventoryFormDto = new InventoryFormDto();
        inventoryFormDto.setClosed(false);
        inventoryFormDto.setDescription("Description ...");
        inventoryFormDto.setName("Inventaire #2");
        inventoryFormDto.setProducts(m);

        InventoryDto inventoryDto = inventoryService.update(new Long(1), inventoryFormDto);

        assertEquals(inventoryDto.getName(), inventoryFormDto.getName());
        assertEquals(inventoryDto.getDescription(), inventoryFormDto.getDescription());
    }

    /* -------- False values -------- */

    /* -------- Exception values -------- */

    @Test(expected = Exception.class)
    public void testFindByIdException() throws Exception {
        InventoryDto inventoryDto = inventoryService.findById(new Long(4));
    }

    @Test(expected = Exception.class)
    public void testFindMyInventoriesByWarehouseIdException() throws Exception {
        List<InventoryDto> inventoryDto = inventoryService.findMyInventoriesByWarehouseId(new Long(5));
    }

    @Test(expected = Exception.class)
    public void testFindByWarehouseIdByIdException() throws Exception {
        List<InventoryDto> inventoryDto = inventoryService.findByWarehouseId(new Long(6));
    }

    /*
    @Test(expected = Exception.class)
    public void testFindLatestByWarehouseIdByIdException() throws Exception {
        Map<ProductDto, Long> inventoryDto = inventoryService.findInventoryProductsById(new Long(7));
    }*/

    @Test(expected = Exception.class)
    public void testCreateException() throws Exception {
        Map<Long, Long> m = new HashMap<>();
        m.put(new Long(1), new Long(1));

        InventoryFormDto inventoryFormDto = new InventoryFormDto();
        inventoryFormDto.setClosed(false);
        inventoryFormDto.setDescription("Description ...");
        inventoryFormDto.setName("Inventaire #2");
        inventoryFormDto.setProducts(m);

        InventoryDto inventoryDto = inventoryService.create(inventoryFormDto, new Long(8));
    }

    @Test(expected = Exception.class)
    public void testUpdateException() throws Exception {
        Map<Long, Long> m = new HashMap<>();
        m.put(new Long(1), new Long(1));

        InventoryFormDto inventoryFormDto = new InventoryFormDto();
        inventoryFormDto.setClosed(false);
        inventoryFormDto.setDescription("Description ...");
        inventoryFormDto.setName("Inventaire #2");
        inventoryFormDto.setProducts(m);

        InventoryDto inventoryDto = inventoryService.update(new Long(3), inventoryFormDto);
    }
}
