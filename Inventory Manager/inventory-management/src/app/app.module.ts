import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { WarehouseComponent } from './components/warehouse/warehouse.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MyInventoriesComponent } from './components/user/my-inventories/my-inventories.component';
import { ProductsComponent } from './components/admin/products/products.component';
import { InventoriesComponent } from './components/admin/inventories/inventories.component';
import { UsersComponent } from './components/admin/users/users.component';
import { CreateInventoryDialogComponent } from './components/user/my-inventories/create-inventory-dialog/create-inventory-dialog.component';
import { ProductsEditDialogComponent } from './components/admin/products/products-edit-dialog/products-edit-dialog.component';
import { EditInventoryDialogComponent } from './components/user/my-inventories/edit-inventory-dialog/edit-inventory-dialog.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { WarehouseDialogComponent } from './pages/main/warehouse-dialog/warehouse-dialog.component';
import { ProductsDialogComponent } from './components/admin/products/products-dialog/products-dialog.component';
import { ShowInventoryDialogComponent } from './components/dialogs/show-inventory-dialog/show-inventory-dialog.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './shared/helpers/token.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    WarehouseComponent,
    ProfileComponent,
    ProductsComponent,
    InventoriesComponent,
    UsersComponent,
    MyInventoriesComponent,
    CreateInventoryDialogComponent,
    WarehouseDialogComponent,
    ProductsDialogComponent,
    ShowInventoryDialogComponent,
    ProductsEditDialogComponent,
    EditInventoryDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatAutocompleteModule,
    HttpClientModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }],
  bootstrap: [AppComponent],
  entryComponents: [
    CreateInventoryDialogComponent,
    EditInventoryDialogComponent,
    WarehouseDialogComponent,
    ProductsDialogComponent,
    ShowInventoryDialogComponent,
    ProductsEditDialogComponent,
  ],
})

export class AppModule { }
