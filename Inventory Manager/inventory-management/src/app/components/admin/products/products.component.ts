import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Product } from 'src/app/shared/models/product';
import { Subscription } from 'rxjs';
import { ProductService } from 'src/app/shared/services/product.service';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { ProductsEditDialogComponent } from './products-edit-dialog/products-edit-dialog.component';
import { ProductsDialogComponent } from './products-dialog/products-dialog.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  displayedColumns: string[] = ['name', 'description', 'quantity', 'actions'];
  dataSource: MatTableDataSource<Product>;

  initSubscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private productService: ProductService, private warehouseService: WarehouseService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.initSubscription = this.warehouseService.selectedWarehouse$.subscribe(warehouse => {
      if (warehouse == null) {
        this.warehouseService.forceUpdateSelectedWarehouse();
      } else {
        this.safeInit();
      }
    });
  }

  safeInit() {
    if (this.initSubscription) {
      this.initSubscription.unsubscribe();
    }

    this.productService.getAll(this.warehouseService.selectedWarehouse.getValue()).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onCreateProduct() {
    const dialog = this.dialog.open(ProductsDialogComponent, {
      width: '400px',
      disableClose: true,
    });

    dialog.afterClosed().subscribe(() => {
      if (dialog.componentInstance.createdProduct != null) {
        this.dataSource.data.push(dialog.componentInstance.createdProduct);
        this.dataSource.data = [...this.dataSource.data];
      }
    });
  }

  onEditProduct(product: Product) {
    const dialog = this.dialog.open(ProductsEditDialogComponent, {
      width: '400px',
      disableClose: true,
      data: product
    });

    dialog.afterClosed().subscribe(() => {
      let updatedProduct = dialog.componentInstance.product;

      if (product != null) {
        Object.assign(this.dataSource.data.find(p => p.id === updatedProduct.id), updatedProduct);
      }
    });
  }

  onDeleteProduct(product: Product) {
    this.productService.delete(product).subscribe(
      _ => {
        this.dataSource.data.splice(this.dataSource.data.indexOf(product), 1);
        this.dataSource.data = [...this.dataSource.data];
      }
    );
  }
}
