import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Product } from 'src/app/shared/models/product';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-products-edit-dialog',
  templateUrl: './products-edit-dialog.component.html',
  styleUrls: ['./products-edit-dialog.component.css']
})
export class ProductsEditDialogComponent implements OnInit {
  product: Product = new Product();

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl()
  });

  constructor(public thisDialogRef: MatDialogRef<ProductsEditDialogComponent>, @Inject(MAT_DIALOG_DATA) data, private productService: ProductService) {
    Object.assign(this.product, data);
  }

  ngOnInit() {
    this.controls.name.setValue(this.product.name);
    this.controls.description.setValue(this.product.description);
  }

  get controls() {
    return this.form.controls;
  }

  onEditProduct() {
    if (this.form.invalid) {
      return;
    }

    let product : Product = new Product();
    Object.assign(product, this.product);

    product.name = this.controls.name.value;
    product.description = this.controls.description.value;

    this.productService.update(product).subscribe(
      res => {
        this.product = res;
        this.thisDialogRef.close();
      }
    );
  }
}
