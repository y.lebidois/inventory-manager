import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { Warehouse } from 'src/app/shared/models/warehouse';
import { Product } from 'src/app/shared/models/product';

@Component({
  selector: 'app-products-dialog',
  templateUrl: './products-dialog.component.html',
  styleUrls: ['./products-dialog.component.css']
})
export class ProductsDialogComponent implements OnInit {
  warehouse: Warehouse;
  createdProduct: Product;

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl()
  });

  constructor(private productService: ProductService, private warehouseService: WarehouseService, public thisDialogRef: MatDialogRef<ProductsDialogComponent>) {
    this.warehouseService.selectedWarehouse$.subscribe(warehouse => this.warehouse = warehouse);
  }

  ngOnInit() {
  }

  get controls() {
    return this.form.controls;
  }

  onCreateProduct() {
    if (this.form.invalid) {
      return;
    }

    let product = new Product();
    product.name = this.controls.name.value;
    product.description = this.controls.description.value;

    this.productService.create(this.warehouse, product)
      .subscribe(
        res => {
          this.createdProduct = res;
          this.thisDialogRef.close();
        }
      );
  }
}
