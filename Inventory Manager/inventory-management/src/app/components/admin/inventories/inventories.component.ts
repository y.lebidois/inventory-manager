import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { ShowInventoryDialogComponent } from 'src/app/components/dialogs/show-inventory-dialog/show-inventory-dialog.component';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { Inventory } from 'src/app/shared/models/inventory';
import { InventoryService } from 'src/app/shared/services/inventory.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.css']
})
export class InventoriesComponent implements OnInit {
  displayedColumns: string[] = ['date', 'name', 'description', 'author', 'alert', 'actions'];
  dataSource: MatTableDataSource<Inventory>;

  initSubscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private inventoryService: InventoryService, private warehouseService: WarehouseService, private alertService: AlertService, public dialog: MatDialog) { }

  ngOnInit() {
    this.initSubscription = this.warehouseService.selectedWarehouse$.subscribe(warehouse => {
      if (warehouse == null) {
        this.warehouseService.forceUpdateSelectedWarehouse();
      } else {
        this.safeInit();
      }
    });
  }

  safeInit() {
    if (this.initSubscription) {
      this.initSubscription.unsubscribe();
    }

    this.inventoryService.getAll(this.warehouseService.selectedWarehouse.getValue()).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onShowInventory(inventory: Inventory) {
    this.dialog.open(ShowInventoryDialogComponent, {
      width: '600px',
      disableClose: true,
      data: inventory
    });
  }

  onExportInventory(inventory: Inventory) {
    this.inventoryService.getFullInformation(inventory).subscribe(
      res => {
        let blob = new Blob([JSON.stringify(res, null, 2)], { type: 'application/json' });
        FileSaver.saveAs(blob, `inventaire_${inventory.name}.json`);
      }
    );
  }
}
