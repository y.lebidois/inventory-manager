import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { WarehousePermission } from 'src/app/shared/models/warehouse-permission';
import { UserService } from 'src/app/shared/services/user.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import * as FileSaver from 'file-saver';

interface UserPermissionData {
  firstName: string;
  lastName: string;
  email: string;
  admin: boolean;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'admin', 'actions'];
  dataSource: MatTableDataSource<UserPermissionData>;
  email = new FormControl('', [Validators.required, Validators.email]);

  initSubscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private warehouseService: WarehouseService, private userService: UserService, private alertService: AlertService) { }

  ngOnInit() {
    this.initSubscription = this.warehouseService.selectedWarehouse$.subscribe(warehouse => {
      if (warehouse == null) {
        this.warehouseService.forceUpdateSelectedWarehouse();
      } else {
        this.safeInit();
      }
    });
  }

  safeInit() {
    if (this.initSubscription) {
      this.initSubscription.unsubscribe();
    }

    this.warehouseService.getPermissions(this.warehouseService.selectedWarehouse.getValue()).subscribe(
      res => {
        const permissions = [];

        res.forEach(permission => {
          permissions.push(createNewPermission(permission));
        })

        this.dataSource = new MatTableDataSource(permissions);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  onAddPermission() {
    if (!this.email.valid) {
      return;
    }

    this.userService.findByUsername(this.email.value)
      .subscribe(
        res => {
          // Prevent duplicates
          if (this.dataSource.data.some(user => user.email === this.email.value)) {
            this.alertService.displayMessage("Cet utilisateur a déjà été ajouté");
            return;
          }

          let permission = new WarehousePermission();
          permission.user = res;
          permission.admin = false;

          this.warehouseService.addPermission(this.warehouseService.selectedWarehouse.getValue(), permission).subscribe(
            _ => {
              this.dataSource.data.push(createNewPermission(permission));
              this.dataSource.data = [...this.dataSource.data];

              this.email.clearValidators();
              this.email.setValue('');
              this.email.setValidators([Validators.required, Validators.email]);
            }
          );
        }
      );
  }

  onUpdatePermission(data: UserPermissionData) {
    const permission = {
      user: {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email
      },
      admin: !data.admin
    };

    this.warehouseService.updatePermission(this.warehouseService.selectedWarehouse.getValue(), permission).subscribe(
      res =>  data.admin = res.admin
    )
  }

  onDeletePermission(data: UserPermissionData) {
    const permission = {
      user: {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email
      },
      admin: data.admin
    };

    this.warehouseService.deletePermission(this.warehouseService.selectedWarehouse.getValue(), permission).subscribe(
      _ => {
        this.dataSource.data.splice(this.dataSource.data.indexOf(data), 1);
        this.dataSource.data = [...this.dataSource.data];
      }
    )
  }

  onExportUsers() {
      let blob = new Blob([JSON.stringify(this.dataSource.data, null, 2)], { type: 'application/json' });
      FileSaver.saveAs(blob, `warehouse_${this.warehouseService.selectedWarehouse.getValue().name}_users.json`);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

function createNewPermission(permission: WarehousePermission): UserPermissionData {
  return {
    firstName: permission.user.firstName,
    lastName: permission.user.lastName,
    email: permission.user.email,
    admin: permission.admin
  };
}
