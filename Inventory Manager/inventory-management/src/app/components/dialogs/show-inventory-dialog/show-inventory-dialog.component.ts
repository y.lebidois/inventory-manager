import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { InventoryService } from 'src/app/shared/services/inventory.service';
import { InventoryProduct } from 'src/app/shared/models/inventory-product';

@Component({
  selector: 'app-show-inventory-dialog',
  templateUrl: './show-inventory-dialog.component.html',
  styleUrls: ['./show-inventory-dialog.component.css']
})
export class ShowInventoryDialogComponent implements OnInit {

  displayedColumns: string[] = ['name', 'quantity', 'outOfStock'];
  dataSource: MatTableDataSource<InventoryProduct>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public thisDialogRef: MatDialogRef<ShowInventoryDialogComponent>, @Inject(MAT_DIALOG_DATA) data, private inventoryService: InventoryService) {
    this.inventoryService.getFullInformation(data).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res.products);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  ngOnInit() {
  }

  onClose() {
    this.thisDialogRef.close();
  }
}