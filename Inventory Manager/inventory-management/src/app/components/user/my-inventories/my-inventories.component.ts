import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { CreateInventoryDialogComponent } from './create-inventory-dialog/create-inventory-dialog.component';
import { EditInventoryDialogComponent } from './edit-inventory-dialog/edit-inventory-dialog.component';
import { ShowInventoryDialogComponent } from 'src/app/components/dialogs/show-inventory-dialog/show-inventory-dialog.component';
import { InventoryService } from 'src/app/shared/services/inventory.service';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { Inventory } from 'src/app/shared/models/inventory';

@Component({
  selector: 'app-my-inventories',
  templateUrl: './my-inventories.component.html',
  styleUrls: ['./my-inventories.component.css']
})

export class MyInventoriesComponent implements OnInit {
  displayedColumns: string[] = ['date', 'name', 'description', 'state', 'actions'];
  dataSource: MatTableDataSource<Inventory>;

  initSubscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private warehouseService: WarehouseService, private inventoryService: InventoryService, public dialog: MatDialog) { }

  ngOnInit() {
    this.initSubscription = this.warehouseService.selectedWarehouse$.subscribe(warehouse => {
      if (warehouse == null) {
        this.warehouseService.forceUpdateSelectedWarehouse();
      } else {
        this.safeInit();
      }
    });
  }

  safeInit() {
    if (this.initSubscription) {
      this.initSubscription.unsubscribe();
    }

    this.inventoryService.getMyInventories(this.warehouseService.selectedWarehouse.getValue()).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onCreateInventory() {
    const dialog = this.dialog.open(CreateInventoryDialogComponent, {
      width: '800px',
      disableClose: true,
    });

    dialog.afterClosed().subscribe(() => {
      if (dialog.componentInstance.inventory != null) {
        this.dataSource.data.unshift(dialog.componentInstance.inventory);
        this.dataSource.data = [...this.dataSource.data];
      }
    });
  }

  onEditInventory(inventory: Inventory) {
    const dialog = this.dialog.open(EditInventoryDialogComponent, {
      width: '800px',
      data: inventory,
      disableClose: true,
    });

    dialog.afterClosed().subscribe(() => {
      let inventory = dialog.componentInstance.inventory;

      if (inventory != null) {
        Object.assign(this.dataSource.data.find(p => p.id === inventory.id), inventory);
      }
    });
  }

  onDeleteInventory(inventory : Inventory) {
    this.inventoryService.delete(inventory).subscribe(
      _ => {
        this.dataSource.data.splice(this.dataSource.data.indexOf(inventory), 1);
        this.dataSource.data = [...this.dataSource.data];
      }
    );
  }

  onShowInventory(inventory: Inventory) {
    this.dialog.open(ShowInventoryDialogComponent, {
      width: '600px',
      data: inventory,
      disableClose: true,
    });
  }
}
