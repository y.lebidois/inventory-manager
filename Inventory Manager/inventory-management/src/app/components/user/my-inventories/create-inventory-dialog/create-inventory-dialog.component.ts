import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { InventoryService } from 'src/app/shared/services/inventory.service';
import { ProductService } from 'src/app/shared/services/product.service';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { Product } from 'src/app/shared/models/product';
import { Inventory } from 'src/app/shared/models/inventory';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-create-inventory-dialog',
  templateUrl: './create-inventory-dialog.component.html',
  styleUrls: ['./create-inventory-dialog.component.css']
})

export class CreateInventoryDialogComponent implements OnInit {
  inventory: Inventory;

  displayedColumns: string[] = ['name', 'quantity', 'actions'];
  dataSource: MatTableDataSource<Product>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  inventoryForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl()
  });

  productForm = new FormGroup({
    productName: new FormControl(),
    productQuantity: new FormControl(),
    confirmPassword: new FormControl()
  });

  products: Product[];
  filteredProducts: Observable<Product[]>;

  constructor(private inventoryService: InventoryService,
    private productService: ProductService,
    private warehouseService: WarehouseService,
    public thisDialogRef: MatDialogRef<CreateInventoryDialogComponent>,
    private alertService: AlertService) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.productService.getAll(this.warehouseService.selectedWarehouse.getValue()).subscribe(
      res => {
        this.products = res;

        this.filteredProducts = this.productControls.productName.valueChanges.pipe(
          startWith<string | Product>(''),
          map(value => typeof value === 'string' ? value : value.name),
          map(name => name ? this._filter(name) : this.products.slice())
        );
      }
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  get inventoryControls() {
    return this.inventoryForm.controls;
  }

  get productControls() {
    return this.productForm.controls;
  }

  onCreateInventory() {
    this._createInventory(false);
  }

  onCreateAndCloseInventory() {
    this._createInventory(true);
  }

  onAddProduct() {
    if (this.productForm.invalid) {
      return;
    }

    let product = this.productControls.productName.value;

    if (typeof this.productControls.productName.value === 'string') {
      product = this.products.find(p => p.name.toLowerCase() == this.productControls.productName.value.toString().toLowerCase());
    }

    if (product == null) {
      this.alertService.displayMessage("Ce produit n'existe pas");
      return;
    }

    if (this.dataSource.data.some(p => p.id == product.id)) {
      this.alertService.displayMessage("Ce produit existe déjà");
      return;
    }
    
    product.currentQuantity = Math.max(0, this.productControls.productQuantity.value);

    this.dataSource.data.push(product);
    this.dataSource.data = [...this.dataSource.data];

    this.productControls.productName.setValue('');
    this.productControls.productQuantity.setValue('');
  }

  onDeleteProduct(product: Product) {
    this.dataSource.data.splice(this.dataSource.data.indexOf(product), 1);
    this.dataSource.data = [...this.dataSource.data];
  }

  displayFn(product?: Product): string | undefined {
    return product ? product.name : undefined;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private _filter(name: string): Product[] {
    const filterValue = name.toLowerCase();
    return this.products.filter(product => product.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private _createInventory(closed: boolean) {
    if (this.inventoryForm.invalid) {
      return;
    }

    const inventory = new Inventory();
    inventory.name = this.inventoryControls.name.value;
    inventory.description = this.inventoryControls.description.value;
    inventory.products = {};
    inventory.closed = closed;
    
    this.dataSource.data.forEach(p => {
      inventory.products[p.id] = p.currentQuantity;
    });

    this.inventoryService.create(this.warehouseService.selectedWarehouse.getValue(), inventory).subscribe(
      res => {
        this.inventory = res;
        this.thisDialogRef.close();
      }
    );
  }
}