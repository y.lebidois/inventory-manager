import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { InventoryService } from 'src/app/shared/services/inventory.service';
import { ProductService } from 'src/app/shared/services/product.service';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { Product } from 'src/app/shared/models/product';
import { Inventory } from 'src/app/shared/models/inventory';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-edit-inventory-dialog',
  templateUrl: './edit-inventory-dialog.component.html',
  styleUrls: ['./edit-inventory-dialog.component.css']
})

export class EditInventoryDialogComponent implements OnInit {
  inventory: Inventory;

  displayedColumns: string[] = ['name', 'quantity', 'actions'];
  dataSource: MatTableDataSource<Product>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  inventoryForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl()
  });

  productForm = new FormGroup({
    productName: new FormControl(),
    productQuantity: new FormControl(),
    confirmPassword: new FormControl()
  });

  products: Product[];
  filteredProducts: Observable<Product[]>;

  constructor(private inventoryService: InventoryService,
    private productService: ProductService,
    private warehouseService: WarehouseService,
    public thisDialogRef: MatDialogRef<EditInventoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private alertService: AlertService) {

      this.inventory = data;
      
      this.inventoryService.getFullInformation(data).subscribe(
        res => {
          this.dataSource = new MatTableDataSource();

          res.products.forEach(p => {
            this.dataSource.data.push({
              id: p.product.id,
              name: p.product.name,
              description: p.product.description,
              currentQuantity: p.quantity
            });
          })

          this.inventoryControls.name.setValue(data.name);
          this.inventoryControls.description.setValue(data.description);

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          
          this.productService.getAll(this.warehouseService.selectedWarehouse.getValue()).subscribe(
            res => {
              this.products = res;
      
              this.filteredProducts = this.productControls.productName.valueChanges.pipe(
                startWith<string | Product>(''),
                map(value => typeof value === 'string' ? value : value.name),
                map(name => name ? this._filter(name) : this.products.slice())
              );
            }
          );
        }
      );
  }

  ngOnInit() {}

  get inventoryControls() {
    return this.inventoryForm.controls;
  }

  get productControls() {
    return this.productForm.controls;
  }

  onUpdateInventory() {
    this._updateInventory(false);
  }

  onUpdateAndCloseInventory() {
    this._updateInventory(true);
  }

  onAddProduct() {
    if (this.productForm.invalid) {
      return;
    }

    let product = this.productControls.productName.value;

    if (typeof this.productControls.productName.value === 'string') {
      product = this.products.find(p => p.name.toLowerCase() == this.productControls.productName.value.toString().toLowerCase());
    }

    if (product == null) {
      this.alertService.displayMessage("Ce produit n'existe pas");
      return;
    }

    if (this.dataSource.data.some(p => p.id == product.id)) {
      this.alertService.displayMessage("Ce produit existe déjà");
      return;
    }
    
    product.currentQuantity = Math.max(0, this.productControls.productQuantity.value);

    this.dataSource.data.push(product);
    this.dataSource.data = [...this.dataSource.data];

    this.productControls.productName.setValue('');
    this.productControls.productQuantity.setValue('');
  }

  onDeleteProduct(product: Product) {
    this.dataSource.data.splice(this.dataSource.data.indexOf(product), 1);
    this.dataSource.data = [...this.dataSource.data];
  }

  displayFn(product?: Product): string | undefined {
    return product ? product.name : undefined;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private _filter(name: string): Product[] {
    const filterValue = name.toLowerCase();
    return this.products.filter(product => product.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private _updateInventory(closed: boolean) {
    if (this.inventoryForm.invalid) {
      return;
    }

    this.inventory.name = this.inventoryControls.name.value;
    this.inventory.description = this.inventoryControls.description.value;
    this.inventory.closed = closed;

    this.inventory.products = {};
    this.dataSource.data.forEach(p => {
      this.inventory.products[p.id] = p.currentQuantity;
    });

    this.inventoryService.update(this.inventory).subscribe(
      _ => {
        this.thisDialogRef.close();
      }
    );
  }
}