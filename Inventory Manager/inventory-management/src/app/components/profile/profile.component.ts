import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User = new User();

  hide1 = true;
  hide2 = true;
  hide3 = true;

  passwordForm = new FormGroup({
    currentPassword: new FormControl(),
    password: new FormControl(),
    confirmPassword: new FormControl()
  });

  constructor(private userService: UserService, private warehouseService: WarehouseService) { }

  ngOnInit() {
    this.userService.getProfile().subscribe(user => this.user = user);
  }

  onProfileInformationChange() {
    this.userService.updateProfileInformation(this.user)
      .subscribe(
        _ => location.reload()
      );
  }

  onProfilePasswordChange() {
    this.userService.updateProfilePassword(this.controls.currentPassword.value, this.controls.password.value, this.controls.confirmPassword.value)
      .subscribe(
        _ => location.reload()
      );
  }

  get controls() {
    return this.passwordForm.controls;
  }
}
