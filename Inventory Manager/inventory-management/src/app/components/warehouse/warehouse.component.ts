import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Warehouse } from '../../shared/models/warehouse';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { WarehouseDialogComponent } from 'src/app/pages/main/warehouse-dialog/warehouse-dialog.component';
import { WarehouseStatistics } from 'src/app/shared/models/warehouse-statistics';
import { InventoryService } from 'src/app/shared/services/inventory.service';
import { ShowInventoryDialogComponent } from 'src/app/components/dialogs/show-inventory-dialog/show-inventory-dialog.component';
import { Inventory } from 'src/app/shared/models/inventory';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css']
})

export class WarehouseComponent implements OnDestroy, OnInit {
  statistics = new WarehouseStatistics();

  warehouse: Warehouse;
  subscription: Subscription;
  initSubscription: Subscription;

  displayedColumns: string[] = ['date', 'name', 'description', 'author', 'alert', 'actions'];
  dataSource: MatTableDataSource<Inventory>;

  constructor(private warehouseService: WarehouseService, private inventoryService: InventoryService, public dialog: MatDialog, private route: ActivatedRoute) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.initSubscription = this.warehouseService.selectedWarehouse$.subscribe(warehouse => {
      if (warehouse == null) {
        this.warehouseService.forceUpdateSelectedWarehouse();
      } else {
        this.safeInit();
      }
    });
  }

  safeInit() {
    if (this.initSubscription) {
      this.initSubscription.unsubscribe();
    }

    this.subscription = this.warehouseService.selectedWarehouse$.subscribe(warehouse => {
      this.warehouse = warehouse;

      this.warehouseService.getStatistics(this.warehouse.id)
        .subscribe(
          res => {
            this.statistics = res;
          }
        );

      this.inventoryService.getLatest(this.warehouse)
        .subscribe(
          res => {
            this.dataSource.data = res;
            this.dataSource.data = [...this.dataSource.data];
          }
        );
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onShowInventory(inventory: Inventory) {
    this.dialog.open(ShowInventoryDialogComponent, {
      width: '600px',
      data: inventory,
      disableClose: true,
    });
  }
}
