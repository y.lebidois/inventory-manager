import { User } from './user';

export class Warehouse {
    id: number;
    name: string;
    creator: User;
    admin: boolean;

    getPrefix(): string {
        return this.name.substring(0, 2);
    }
}