import { User } from './user';

class Inventory {
    id: number;
    name: string;
    description: string;
    author: User;
    date: Date;
    products: {};
    closed: boolean;
    outOfStock: boolean;
}

export { Inventory }