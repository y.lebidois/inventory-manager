import { Product } from './product';

class InventoryProduct {
    product: Product;
    quantity: number;
}

export { InventoryProduct }