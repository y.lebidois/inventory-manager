import { User } from './user';

export class WarehousePermission {
    user: User;
    admin: boolean;
}