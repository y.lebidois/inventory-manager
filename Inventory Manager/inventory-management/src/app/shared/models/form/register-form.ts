export class RegisterForm {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
}