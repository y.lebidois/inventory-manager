
export class WarehouseStatistics {
    status: string;
    usersCount: number;
    inventoriesClosedCount: number;
    outOfStockCount: number;
    lastClosedDate : Date;
    pendingInventoriesCount: number;
    productsCount: number;
    myInventoriesCount: number;
}