import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {environment} from 'src/environments/environment';
import {catchError} from 'rxjs/operators';
import {AlertService} from '../services/alert.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public authService: AuthService, private alertService: AlertService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // If the user is authenticated and try to request secured routes
    if (request.url.startsWith(`${environment.apiUrl}`)) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.authService.getToken()}`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.authService.logout();
        }
        // If there is en error with a simple message
        else if (error.error.message) {
          this.alertService.displayMessage(error.error.message);
        }
        // If there is en error with a description
        else if (error.error.error_description) {
		  let message = error.error.error == 'invalid_grant' ? 'Identifiant ou mot de passe incorrect' : error.error.error_description;
          this.alertService.displayMessage(message);
        }
        else {
          this.alertService.displayMessage("Une erreur est survenue");
        }

        return throwError(error);
      })
    );
  }
}
