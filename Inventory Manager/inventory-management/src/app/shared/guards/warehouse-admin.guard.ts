import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { WarehouseService } from '../services/warehouse.service';

@Injectable({
    providedIn: 'root'
})
export class WarehouseAdminGuard implements CanActivate {

    constructor(private warehouseService: WarehouseService, private router: Router) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return true;
    }
}