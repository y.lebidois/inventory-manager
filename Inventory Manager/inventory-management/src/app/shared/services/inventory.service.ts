import { Warehouse } from '../models/warehouse';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Inventory } from '../models/inventory';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})

export class InventoryService {
  constructor(private http: HttpClient) { }

  getMyInventories(warehouse: Warehouse) {
    return this.http.get<Inventory[]>(`${environment.apiUrl}/inventories/mine?fromWarehouse=${warehouse.id}`);
  }

  getAll(warehouse: Warehouse) {
    return this.http.get<Inventory[]>(`${environment.apiUrl}/inventories?fromWarehouse=${warehouse.id}`);
  }

  getLatest(warehouse: Warehouse) {
    return this.http.get<Inventory[]>(`${environment.apiUrl}/inventories/latest?fromWarehouse=${warehouse.id}`);
  }

  getFullInformation(inventory: Inventory) {
    return this.http.get<any>(`${environment.apiUrl}/inventories/${inventory.id}/products`);
  }

  create(warehouse: Warehouse, inventory: Inventory) {
    return this.http.post<Inventory>(`${environment.apiUrl}/inventories?inWarehouse=${warehouse.id}`, inventory);
  }

  update(inventory: Inventory) {
    return this.http.put<Inventory>(`${environment.apiUrl}/inventories/${inventory.id}`, inventory);
  }

  delete(inventory: Inventory) {
    return this.http.delete<any>(`${environment.apiUrl}/inventories/${inventory.id}`);
  }
}