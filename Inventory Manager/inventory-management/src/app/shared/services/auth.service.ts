import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterForm } from 'src/app/shared/models/form/register-form';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Authorization': 'Basic ' + btoa('inventory_manager_client_id:inventory_manager_client_secret') })
};

@Injectable({ providedIn: 'root' })

export class AuthService {
  redirectUrl: string;

  constructor(private http: HttpClient, private router: Router) { }

  login(username: string, password: string) {
    let formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    formData.append('grant_type', 'password');

    return this.http.post<any>(`${environment.url}/oauth/token`, formData, httpOptions).pipe(
      tap(res => localStorage.setItem('access_token', res.access_token))
    );
  }

  register(registration: RegisterForm) {
    return this.http.post<any>(`${environment.url}/auth/signup`, registration);
  }

  logout() {
    localStorage.removeItem('access_token');
    this.router.navigate(['/login']);
  }

  getToken(): string {
    return localStorage.getItem('access_token');
  }
}