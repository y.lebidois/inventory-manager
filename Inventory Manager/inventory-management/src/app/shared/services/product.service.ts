import { Warehouse } from '../models/warehouse';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';

@Injectable({
    providedIn: 'root'
})

export class ProductService {
    constructor(private http: HttpClient) { }

    create(warehouse: Warehouse, product: Product) {
        return this.http.post<Product>(`${environment.apiUrl}/products?inWarehouse=${warehouse.id}`, product);
    }

    update(product: Product) {
        return this.http.put<any>(`${environment.apiUrl}/products/${product.id}`, product);
    }

    delete(product: Product) {
        return this.http.delete<any>(`${environment.apiUrl}/products/${product.id}`);
    }

    getAll(warehouse: Warehouse) {
        return this.http.get<Product[]>(`${environment.apiUrl}/products?fromWarehouse=${warehouse.id}`);
    }
}