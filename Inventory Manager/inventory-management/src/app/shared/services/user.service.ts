import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    constructor(private http: HttpClient) { }

    findByUsername(username: string) {
        return this.http.get<User>(`${environment.apiUrl}/users?email=${username}`);
    }

    getProfile() {
        return this.http.get<User>(`${environment.apiUrl}/users/profile`);
    }

    updateProfileInformation(user: User) {
        return this.http.put<any>(`${environment.apiUrl}/users/profile/information`, user);
    }

    updateProfilePassword(currentPassword: string, password: string, confirmPassword: string) {
        return this.http.put<any>(`${environment.apiUrl}/users/profile/password`,
            {
                "currentPassword": currentPassword,
                "password": password,
                "confirmPassword": confirmPassword
            });
    }
}