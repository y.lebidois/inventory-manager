import { BehaviorSubject } from 'rxjs';
import { Warehouse } from '../models/warehouse';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WarehouseForm } from '../models/form/warehouse-form';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { WarehouseStatistics } from '../models/warehouse-statistics';
import { WarehousePermission } from '../models/warehouse-permission';

@Injectable({
  providedIn: 'root'
})

export class WarehouseService {
  selectedWarehouse = new BehaviorSubject<Warehouse>(null);
  selectedWarehouse$ = this.selectedWarehouse.asObservable();

  constructor(private http: HttpClient, private router: Router) { }

  create(warehouse: WarehouseForm) {
    return this.http.post<Warehouse>(`${environment.apiUrl}/warehouses`, warehouse);
  }

  getStatistics(id: number) {
    return this.http.get<WarehouseStatistics>(`${environment.apiUrl}/warehouses/${id}/statistics`);
  }

  getWarehouses() {
    return this.http.get<Warehouse[]>(`${environment.apiUrl}/warehouses`);
  }

  getPermissions(warehouse: Warehouse) {
    return this.http.get<WarehousePermission[]>(`${environment.apiUrl}/warehouses/${warehouse.id}/permissions`);
  }

  addPermission(warehouse: Warehouse, permission: WarehousePermission) {
    return this.http.post<WarehousePermission>(`${environment.apiUrl}/warehouses/${warehouse.id}/permissions`, permission);
  }

  updatePermission(warehouse: Warehouse, permission: WarehousePermission) {
    return this.http.put<WarehousePermission>(`${environment.apiUrl}/warehouses/${warehouse.id}/permissions`, permission);
  }

  deletePermission(warehouse: Warehouse, permission: WarehousePermission) {
    return this.http.request('DELETE', `${environment.apiUrl}/warehouses/${warehouse.id}/permissions`, { body: permission });
  }

  updateSelectedWarehouse(warehouse: Warehouse) {
    this.selectedWarehouse.next(warehouse);
  }

  forceUpdateSelectedWarehouse() {
    this.getWarehouses().subscribe(
      res => {
        if (this.router.url.split("/")[1] === "warehouse") {
          this.updateSelectedWarehouse(res.find(w => w.id == +this.router.url.split("/")[2]));
        };
      }
    );
  }
}
