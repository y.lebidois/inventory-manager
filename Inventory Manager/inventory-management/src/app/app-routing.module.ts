import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WarehouseComponent} from './components/warehouse/warehouse.component';
import {MyInventoriesComponent} from './components/user/my-inventories/my-inventories.component';
import {ProductsComponent} from './components/admin/products/products.component';
import {InventoriesComponent} from './components/admin/inventories/inventories.component';
import {UsersComponent} from './components/admin/users/users.component';
import {LoginComponent} from './pages/login/login.component';
import {MainComponent} from './pages/main/main.component';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthGuard} from './shared/guards/auth.guard';
import {WarehouseGuard} from './shared/guards/warehouse.guard';
import {WarehouseAdminGuard} from './shared/guards/warehouse-admin.guard';

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: MainComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'profile', canActivate: [AuthGuard], component: MainComponent, children: [
      {path: '', component: ProfileComponent}
    ]
  },
  {
    path: 'warehouse', component: MainComponent, canActivate: [AuthGuard, WarehouseGuard], children: [
      {path: ':id', component: WarehouseComponent},
      {path: ':id/my-inventories', component: MyInventoriesComponent},
      {path: ':id/products', canActivate: [WarehouseAdminGuard], component: ProductsComponent},
      {path: ':id/inventories', canActivate: [WarehouseAdminGuard], component: InventoriesComponent},
      {path: ':id/users', canActivate: [WarehouseAdminGuard], component: UsersComponent}
    ]
  },
  {path: '**', canActivate: [AuthGuard], redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
