import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterForm } from 'src/app/shared/models/form/register-form';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  // Login
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl()
  });

  hide = true;
  error = "";

  // Register
  register = new RegisterForm();
  hideR1 = true;
  hideR2 = true;
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(private authService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
    // If the user is already logged in, just redirect him on home page
    if (this.authService.getToken()) {
      this.router.navigate(['/']);
    }
  }

  onLogin() {
    this.authService.login(this.loginControls.email.value, this.loginControls.password.value).subscribe(
      _ => this.router.navigate([this.authService.redirectUrl ? this.authService.redirectUrl : '/'])
    );
  }

  onRegister() {
    this.authService.register(this.register).subscribe(
      _ => {
        (<HTMLFormElement>document.getElementById("formRegister")).reset();
        this.alertService.displayMessage("Utilisateur crée avec succès !");
      }
    );
  }

  get loginControls() {
    return this.loginForm.controls;
  }
}
