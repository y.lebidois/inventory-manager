import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { WarehouseForm } from 'src/app/shared/models/form/warehouse-form';
import { WarehouseService } from 'src/app/shared/services/warehouse.service';
import { UserService } from 'src/app/shared/services/user.service';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user';

interface UserData {
  name: string;
  email: string;
  isAdmin: boolean;
}

@Component({
  selector: 'app-warehouse-dialog',
  templateUrl: './warehouse-dialog.component.html',
  styleUrls: ['./warehouse-dialog.component.css']
})

export class WarehouseDialogComponent implements OnInit {
  name = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);
  displayedColumns: string[] = ['name', 'email', 'admin', 'actions'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private warehouseService: WarehouseService,
    private userService: UserService,
    private router: Router,
    public thisDialogRef: MatDialogRef<WarehouseDialogComponent>) {

    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onAddUser() {
    if (!this.email.valid) {
      return;
    }

    this.userService.findByUsername(this.email.value)
      .subscribe(
        user => {
          // Prevent duplicates
          if (!this.dataSource.data.some(user => user.email === this.email.value)) {
            this.dataSource.data.push(createUserDataFromUser(user));
            this.dataSource.data = [...this.dataSource.data];
          }

          this.email.clearValidators();
          this.email.setValue('');
          this.email.setValidators([Validators.required, Validators.email]);
        }
      );
  }

  onChangeState(user: UserData) {
    user.isAdmin = !user.isAdmin;
  }

  onDeleteUser(user: UserData) {
    this.dataSource.data.splice(this.dataSource.data.indexOf(user), 1);
    this.dataSource.data = [...this.dataSource.data];
  }

  onCreateWarehouse() {
    const warehouse = new WarehouseForm();

    warehouse.name = this.name.value;
    warehouse.permissions = {};

    this.dataSource.data.forEach(user => {
      warehouse.permissions[user.email] = user.isAdmin ? 'ADMIN' : 'USER';
    })

    this.warehouseService.create(warehouse)
      .subscribe(
        warehouse => {
          let url = '/warehouse/' + warehouse.id;
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(_ => this.router.navigate([url]));
          this.thisDialogRef.close();
        }
      );
  }
}

function createUserDataFromUser(user: User): UserData {
  return {
    name: user.firstName + " " + user.lastName,
    email: user.email,
    isAdmin: false
  };
}
