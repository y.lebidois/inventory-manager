import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Warehouse } from '../../shared/models/warehouse';
import { WarehouseService } from '../../shared/services/warehouse.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { WarehouseDialogComponent } from 'src/app/pages/main/warehouse-dialog/warehouse-dialog.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],

  encapsulation: ViewEncapsulation.None,
})

export class MainComponent implements OnInit {
  warehouses = [];
  selectedWarehouse: Warehouse;

  isIndex = false;
  isProfile = false;

  constructor(private warehouseService: WarehouseService,
    private authService: AuthService,
    private router: Router,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.warehouseService.getWarehouses().subscribe(
      res => {
        res.forEach(w => {
          let warehouse = new Warehouse();

          warehouse.id = w.id;
          warehouse.name = w.name;
          warehouse.admin = w.admin;

          this.warehouses.push(warehouse);
        });
      }
    );

    this.warehouseService.selectedWarehouse$.subscribe(warehouse => this.selectedWarehouse = warehouse);

    this.isIndex = this.router.url === "/";
    this.isProfile = this.router.url === "/profile"
  }

  onSelect(warehouse: Warehouse) {
    this.warehouseService.updateSelectedWarehouse(warehouse);
  }

  onLogout() {
    this.authService.logout();
  }

  openDialog() {
    this.dialog.open(WarehouseDialogComponent, {
      width: '800px',
      disableClose: true,
    });
  }
}
